module.exports = {
    tabWidth: 4,
    semi: true,
    singleQuote: true,
    trailingComma: 'es5',
    printWidth: 80,
    overrides: [
        {
            files: '*.routes.js',
            options: {
                printWidth: 200,
            },
        },
        {
            files: 'structure.utils.js',
            options: {
                printWidth: 80,
            },
        },
    ],
};
