const userController = require('../controllers/users.controller');
const userValidation = require('../validations/users.validation');
const { validateSocket } = require('../utils/errors.utils');

const constantUtils = require('../utils/constant.utils');

const userSocket = (io, socket, user) => {
    socket.on(constantUtils?.SOCKET_EVENTS?.TRACK_PARTNER_LOCATION, (body) => {
        if (
            validateSocket(
                userValidation.trackPartnerLocation,
                constantUtils?.NS?.USERS,
                constantUtils?.SOCKET_EVENTS?.TRACK_PARTNER_LOCATION,
                body,
                socket,
                user
            )
        )
            userController.socketTrackPartnerLocation(io, socket, user, body);
    });
};

module.exports = userSocket;
