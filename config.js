const dotenv = require('dotenv');
dotenv.config();

const config = {};

// DEVELOPMENT PORT 165.232.188.66
// Server Start

config.PORT = process.env.PORT || 3001;
config.MONGO = process.env.MONGO || 'mongodb://localhost:27017/delivery';
config.REDIS = process.env.REDIS || 'redis://localhost:6379';
config.ENVIRONMENT = process.env.NODE_ENV || 'development';
config.SERVICE = process.env.SERVICE || 'ALL';
config.SOCKET_PORT = process.env.SOCKET_PORT || 5001;

module.exports = config;
