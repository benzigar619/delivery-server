/* NPM DEPENDENCIES */
const mongoose = require('mongoose');
const constantUtil = require('../utils/constant.utils');

const translationSchema = new mongoose.Schema({
    languageCode: {
        type: String,
        trim: true,
    },
    languageId: {
        type: mongoose.Schema.Types.ObjectId,
        optional: true,
        ref: 'languages',
    },
    title: {
        type: String,
        trim: true,
    },
    description: {
        type: String,
        trim: true,
    },
});

const userSchema = new mongoose.Schema(
    {
        screenType: {
            type: String,
            trim: true,
            required: true,
            enum: [constantUtil.WALKTHROUGH, constantUtil.HOMESCREEN],
            index: true,
        },
        data: {
            // walkthrough data
            pages: [
                {
                    title: {
                        type: String,
                        trim: true,
                        required: true,
                    },
                    description: {
                        type: String,
                        trim: true,
                        required: true,
                    },
                    image: {
                        type: String,
                        trim: true,
                        required: true,
                    },
                    hexColor: {
                        type: String,
                        trim: true,
                        required: true,
                    },
                    translations: [translationSchema],
                },
            ],
            // Home Screen
            homeContentType: { type: String, enum: ['SIGN_IN', 'SIGN_OUT'] },
            homeContents: [
                {
                    title: {
                        type: String,
                        trim: true,
                        required: true,
                    },
                    type: {
                        type: String,
                        trim: true,
                        required: true,
                    },
                    image: {
                        type: String,
                        trim: true,
                        default: '',
                    },
                    style: {
                        type: String,
                        trim: true,
                        required: true,
                    },
                    scroll: {
                        type: String,
                        trim: true,
                        required: true,
                    },
                    translations: [translationSchema],
                },
            ],
            lastEdited: {
                type: mongoose.Schema.Types.ObjectId,
                required: true,
                ref: 'admins',
            },
        },
    },
    { timestamps: true, versionKey: false }
);

const screen = mongoose.model('screens', userSchema, 'dscreens');

module.exports = screen;
