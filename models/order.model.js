/* NPM DEPENDENCIES */
const mongoose = require('mongoose');

const constantUtils = require('../utils/constant.utils');

const PointSchema = new mongoose.Schema({
    type: {
        type: String,
        enum: ['Point'],
    },
    coordinates: {
        type: [Number],
    },
});

const activityContentSchema = new mongoose.Schema(
    {
        time: {
            type: Date,
        },
        location: {
            lat: Number,
            lng: Number,
        },
        adminId: {
            type: mongoose.Schema.Types.ObjectId,
            optional: true,
            ref: 'admins',
            index: true,
        },
    },
    {
        _id: false,
    }
);

const orderSchema = new mongoose.Schema(
    {
        order: {
            uniqueId: {
                type: String,
                required: true,
                trim: true,
                unique: true,
                index: true,
            },
            orderType: {
                type: String,
                enum: [constantUtils.INSTANT],
                required: true,
                index: true,
            },
            otp: { type: String, required: true, trim: true },
            status: {
                type: String,
                enum: [
                    constantUtils.INITIATED,
                    constantUtils.SCHEDULED,
                    constantUtils.ACCEPTED,
                    constantUtils.DESTINATION_ARRIVED,
                    constantUtils.DELIVERED,
                    constantUtils.USER_CANCELLED,
                    constantUtils.PARTNER_CANCELLED,
                    constantUtils.ADMIN_CANCELLED,
                    constantUtils.EXPIRED,
                ],
                required: true,
                index: true,
            },
            retryCount: {
                type: Number,
            },
            // rating: {
            //     average: Number,
            //     message: String,
            // },
        },
        user: {
            id: {
                type: mongoose.Schema.Types.ObjectId,
                required: true,
                ref: 'users',
                index: true,
            },
            phone: {
                code: {
                    type: String,
                },
                number: {
                    type: String,
                    index: true,
                },
            },
        },
        invoice: {
            estimation: {
                serviceTax: {
                    type: Number,
                },
                deliveryCharge: {
                    type: Number,
                },
                nightFare: {
                    type: Number,
                },
                packaging: {
                    type: Number,
                },
                products: {
                    type: Number,
                },
                total: {
                    type: Number,
                },
            },
            final: {
                serviceTax: {
                    type: Number,
                },
                deliveryCharge: {
                    type: Number,
                },
                nightFare: {
                    type: Number,
                },
                packaging: {
                    type: Number,
                },
                products: {
                    type: Number,
                },
                cancellationCharge: {
                    type: Number,
                },
                total: {
                    type: Number,
                },
            },
        },
        city: {
            id: {
                type: mongoose.Schema.Types.ObjectId,
                required: true,
                index: true,
                ref: 'cities',
            },
            algorithm: {
                type: Object,
                required: true,
            },
            orderCalculation: {
                type: Object,
                required: true,
            },
            currentData: {
                type: Object,
            },
        },
        shop: {
            id: {
                type: mongoose.Schema.Types.ObjectId,
                optional: true,
                index: true,
                ref: 'shops',
            },
            currentData: {
                type: Object,
            },
        },
        activity: {
            initiated: activityContentSchema,
            accepted: activityContentSchema,
            started: activityContentSchema,
            destinationArrived: activityContentSchema,
            delivered: activityContentSchema,
            expired: activityContentSchema,
            userDenied: activityContentSchema,
            partnerCancelled: activityContentSchema,
            userCancelled: activityContentSchema,
            switchedBack: activityContentSchema,
            adminCancelled: activityContentSchema,
        },
        zone: {
            id: {
                type: mongoose.Schema.Types.ObjectId,
                optional: true,
                ref: 'zones',
            },
            currentData: {
                type: Object,
            },
        },
        vehicle: {
            id: {
                type: mongoose.Schema.Types.ObjectId,
                optional: true,
                ref: 'vehicles',
            },
        },
        partner: {
            id: {
                type: mongoose.Schema.Types.ObjectId,
                optional: true,
                index: true,
                ref: 'partners',
            },
            phone: {
                code: {
                    type: String,
                },
                number: {
                    type: String,
                },
            },
            currentData: {
                type: Object,
            },
            requestReceived: [
                {
                    id: {
                        type: mongoose.Schema.Types.ObjectId,
                        optional: true,
                        ref: 'partners',
                    },
                    distance: {
                        type: String,
                    },
                    location: {
                        lat: {
                            type: Number,
                        },
                        lng: {
                            type: Number,
                        },
                    },
                    time: {
                        type: Date,
                    },
                    status: {
                        type: String,
                    },
                },
            ],
        },
        product: {
            // Product Documents (while ordered) will be saved here.
            // So It Should be dynamic
            orderedProducts: [
                {
                    type: Object,
                    required: true,
                },
            ],
        },
        payment: {
            option: {
                type: String,
                enum: [constantUtils.CASH],
                required: true,
            },
            paid: {
                status: {
                    type: String,
                    enum: [
                        constantUtils.PAID,
                        constantUtils.UNPAID,
                        constantUtils.PENDING,
                        constantUtils.REFUND,
                    ],
                    required: true,
                    index: true,
                },
                time: {
                    type: Date,
                },
            },
        },
        address: {
            deliveryAddress: {
                addressHouseNo: { type: String, required: false, trim: true },
                addressDirection: { type: String, required: false, trim: true },
                addressType: { type: String, required: false, trim: true },
                shortAddress: { type: String, required: false, trim: true },
                fullAddress: { type: String, required: true, trim: true },
                lat: { type: Number, required: true },
                lng: { type: Number, required: true },
            },
            deliveryLocation: {
                // For MongoDB Queries
                _id: 0,
                type: PointSchema,
                index: '2dsphere',
                default: {
                    type: 'Point',
                    coordinates: [0, 0],
                },
            },
        },
        ratings: {
            order: {
                rating: Number,
                message: String,
            },
            partner: {
                rating: Number,
                message: String,
            },
            products: [
                new mongoose.Schema(
                    {
                        productId: {
                            type: mongoose.Schema.Types.ObjectId,
                            optional: true,
                            ref: 'products',
                        },
                        rating: Number,
                        message: String,
                        images: [String],
                    },
                    {
                        _id: false,
                    }
                ),
            ],
        },
    },
    { timestamps: true, versionKey: false }
);

const orders = mongoose.model('orders', orderSchema, 'dorders');

module.exports = orders;
