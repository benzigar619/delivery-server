/* NPM DEPENDENCIES */
const mongoose = require('mongoose');

const constantUtil = require('../utils/constant.utils');
const { RatingSchema } = require('./product.model');

const PointSchema = new mongoose.Schema({
    type: {
        type: String,
        enum: ['Point'],
    },
    coordinates: {
        type: [Number],
    },
});

const userSchema = new mongoose.Schema(
    {
        firstName: {
            type: String,
            trim: true,
            default: '',
        },
        lastName: {
            type: String,
            trim: true,
            default: '',
        },
        email: {
            type: String,
            trim: true,
            lowercase: true,
            index: {
                unique: true,
                partialFilterExpression: { email: { $type: 'string' } },
            },
        },
        gender: {
            type: String,
            trim: true,
            default: '',
        },
        phone: {
            code: {
                type: String,
                required: true,
                trim: true,
            },
            number: {
                type: String,
                unique: true,
                required: true,
                trim: true,
            },
        },
        otp: {
            type: String,
            trim: true,
        },
        phoneNumberOtp: {
            type: String,
            trim: true,
        },
        phoneNumberVerifiedTime: {
            type: Date,
        },
        avatar: {
            type: String,
            default: constantUtil.DEFAULTAVATAR,
            trim: true,
        },
        languageCode: {
            type: String,
            default: 'en',
            trim: true,
        },
        password: {
            type: String,
        },
        emailToken: {
            type: String,
        },
        emailVerificationTime: {
            type: Date,
        },
        currencyCode: { type: String, default: 'USD' },
        addressList: [
            {
                placeId: {
                    type: String,
                },
                addressType: {
                    type: String,
                    required: true,
                },
                addressName: {
                    type: String,
                    required: true,
                    lowercase: true,
                },
                fullAddress: {
                    type: String,
                    required: true,
                },
                shortAddress: {
                    type: String,
                    required: true,
                },
                lat: {
                    type: Number,
                    required: true,
                },
                lng: {
                    type: Number,
                    required: true,
                },
            },
        ],
        favoriteProducts: [
            {
                productId: {
                    type: mongoose.Schema.Types.ObjectId,
                    optional: true,
                    ref: 'products',
                },
                favoredTime: {
                    type: Date,
                    default: Date.now(),
                },
            },
        ],
        status: {
            /* INCOMPLETE, ACTIVE, INACTIVE, ARCHIVE */
            type: String,
            required: true,
            trim: true,
            enum: [
                constantUtil.INCOMPLETE,
                constantUtil.ACTIVE,
                constantUtil.INACTIVE,
                constantUtil.ARCHIEVE,
            ],
            default: constantUtil.ACTIVE,
        },
        statusLastEdited: {
            type: mongoose.Schema.Types.ObjectId,
            optional: true,
            ref: 'admins',
        },
        cards: [
            {
                type: { type: String, trim: true, lowercase: true },
                cardNumber: { type: String, trim: true, lowercase: true },
                expiryYear: { type: String, trim: true, lowercase: true },
                expiryMonth: { type: String, trim: true, lowercase: true },
                holderName: { type: String, trim: true, lowercase: true },
                defaultCard: { type: Boolean, default: false },
            },
        ],
        defaultPayment: { type: String, default: constantUtil.PAYMENTCASH },
        scanAndPayQR: { type: String, default: null },
        wallet: {
            availableAmount: { type: Number, default: 0 },
            freezedAmount: { type: Number, default: 0 },
            dueAmount: { type: Number, default: 0 },
            scheduleFreezedAmount: { type: Number, default: 0 },
        },
        trustedContacts: [
            {
                name: { type: String, trim: true },
                phone: {
                    code: { type: String, trim: true },
                    number: { type: String, trim: true },
                },
                reason: {},
            },
        ],
        deviceInfo: [
            {
                deliverySocketId: {
                    type: String,
                    trim: true,
                    default: '',
                },
                deviceId: {
                    type: String,
                    trim: true,
                    default: '',
                },
                socketId: {
                    type: String,
                    trim: true,
                    default: '',
                },
                deviceType: {
                    /* WEB, ANDROID, IOS */
                    type: String,
                    trim: true,
                },
                accessToken: {
                    type: String,
                    trim: true,
                },
                platform: {
                    type: String,
                    trim: true,
                },
                createdAt: {
                    type: 'Date',
                    default: Date.now(),
                },
            },
        ],
        location: {
            _id: 0,
            type: PointSchema,
            index: '2dsphere',
            default: {
                type: 'Point',
                coordinates: [0, 0],
            },
        },
        currentBearing: { type: Number },
        altitude: { type: Number },
        horizontalAccuracy: { type: Number },
        verticalAccuracy: { type: Number },
        speed: { type: Number },
        isLocationUpdated: { type: Boolean, default: false },
        locationUpdatedTime: { type: Date, default: null },
        subscriptionInfo: {
            planId: {
                type: mongoose.Types.ObjectId,
                trim: true,
                default: null,
            },
            isSubscribed: { type: Boolean, default: false },
            subscribedAt: { type: Date, default: null },
            subscribedEndAt: { type: Date, default: null },
            subscribedAtTimestamp: { type: Number, default: null },
            subscribedEndAtTimestamp: { type: Number, default: null },
        },
        bankDetails: [
            {
                accountName: { type: String, default: '' },
                accountNumber: { type: String, default: '' },
                bankName: { type: String, default: '' },
                routingNumber: { type: String, default: '' },
                branchCode: { type: String, default: '' },
                currency: { type: String, default: '' },
                country: { type: String, default: '' },
            },
        ],
        rating: RatingSchema,
    },
    { timestamps: true, versionKey: false }
);

const users = mongoose.model('users', userSchema);

module.exports = users;
