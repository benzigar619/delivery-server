/* NPM DEPENDENCIES */
const mongoose = require('mongoose');

const constantUtils = require('../utils/constant.utils');
const redis = require('../utils/redis.utils');

const locationSchema = new mongoose.Schema(
    {
        type: {
            type: String,
            enum: ['Polygon'],
            required: true,
        },
        coordinates: {
            type: [[[Number]]],
            required: true,
        },
    },
    {
        _id: false,
    }
);

const cancellationSchema = new mongoose.Schema(
    {
        status: {
            type: Boolean,
            required: true,
        },
        threshold: {
            type: Number,
            required: true,
        },
        feeStatus: {
            type: Boolean,
            required: true,
        },
        feeAmount: {
            type: Number,
            min: 0,
            required: true,
        },
        feeAfterMinutes: {
            type: Number,
            min: 0,
            required: true,
        },
    },
    {
        _id: false,
    }
);

const citySchema = new mongoose.Schema(
    {
        locationName: {
            type: String,
            lowercase: true,
            unique: true,
            trim: true,
            required: true,
        },
        currencySymbol: {
            type: String,
            required: true,
            default: '$',
        },
        currencyCode: {
            type: String,
            required: true,
            default: 'USD',
        },
        status: {
            /* INCOMPLETE, ACTIVE, INACTIVE, ARCHIVE */
            type: String,
            required: true,
            trim: true,
            enum: [
                constantUtils.INCOMPLETE,
                constantUtils.ACTIVE,
                constantUtils.INACTIVE,
            ],
            default: constantUtils.ACTIVE,
        },
        algorithm: {
            partnerType: {
                type: String,
                enum: [
                    constantUtils.STOCK_PARTNER,
                    constantUtils.INDEPENDENT_PARTNER,
                    constantUtils.SHOP_PARTNER,
                ],
                required: true,
            },
            notificationType: {
                type: String,
                enum: [
                    constantUtils.ALL,
                    constantUtils.NEAR_BY_ONE_BY_ONE,
                    constantUtils.ONLINE_ONE_BY_ONE,
                ],
                required: true,
            },
            payoutMethod: {
                type: String,
                enum: [constantUtils.INSTANT, constantUtils.BILLING],
                required: true,
            },
            billingDaysInterval: {
                type: Number,
                required: true,
                min: 1,
            },
            acceptPaymentModes: [
                {
                    type: String,
                    enum: [
                        constantUtils.WALLET,
                        constantUtils.CARD,
                        constantUtils.CASH,
                        constantUtils.CREDIT,
                    ],
                    required: true,
                },
            ],
            maxOrdersAtTime: {
                type: Number,
                required: true,
            },
            orderRequestTimeOut: {
                type: Number,
                required: true,
            },
            orderRetryCount: {
                type: Number,
                required: true,
            },
            arriveRadius: {
                type: Number,
                required: true,
            },
            region: {
                zoneOrNearBy: {
                    type: String,
                    enum: [constantUtils.ZONES, constantUtils.NEAR_BY],
                    required: true,
                },
                nearByRadius: {
                    type: Number,
                },
            },
            debitTo: [
                {
                    type: String,
                    enum: [constantUtils.PARTNERS, constantUtils.SHOPS],
                    required: true,
                },
            ],
        },
        orderCalculation: {
            // For Order Calculations
            serviceTaxPercentage: {
                type: Number,
                min: 0,
                max: 100,
                required: true,
            },
            partner: {
                minimumDeliveryCharge: {
                    type: Number,
                    min: 0,
                    required: true,
                },
                farePerDistance: {
                    type: Number,
                    min: 0,
                    required: true,
                },
                siteCommissionPercentage: {
                    type: Number,
                    min: 0,
                    max: 100,
                    required: true,
                },
                cancellation: cancellationSchema,
                // peakFare: {
                //     method: {
                //         type: String,
                //         enum: [constantUtils.MANUAL, constantUtils.AUTOMATIC],
                //         required: true,
                //     },
                //     // start Time and end Time are valid only if method is MANUAL
                //     startTime: {
                //         type: Date,
                //     },
                //     endTime: {
                //         type: Date,
                //     },
                //     minRatio: {
                //         type: Number,
                //     },
                //     maxRatio: {
                //         type: Number,
                //     },
                //     siteCommissionPercentage: {
                //         type: Number,
                //         min: 0,
                //         max: 0,
                //     },
                // },
                nightFare: {
                    status: {
                        type: Boolean,
                    },
                    startTime: {
                        type: Date,
                    },
                    endTime: {
                        type: Date,
                    },
                    fareRatio: {
                        type: Number,
                    },
                    siteCommissionPercentage: {
                        type: Number,
                        min: 0,
                        max: 0,
                    },
                },
            },
            user: {
                cancellation: cancellationSchema,
            },
            shop: {
                siteCommissionPercentage: {
                    type: Number,
                    min: 0,
                    max: 100,
                    required: true,
                },
                packagePerProduct: {
                    type: Number,
                    min: 0,
                    required: true,
                },
            },
        },
        timezone: {
            type: String,
            required: true,
        },
        location: {
            type: locationSchema,
        },
        addedBy: {
            type: mongoose.Schema.Types.ObjectId,
            required: true,
            ref: 'admins',
        },
        lastEdited: {
            type: mongoose.Schema.Types.ObjectId,
            required: true,
            ref: 'admins',
        },
        lastEditedTime: {
            default: Date.now,
            type: Date,
            required: true,
        },
        zones: [{ type: mongoose.Schema.Types.ObjectId, ref: 'zones' }],
    },
    { timestamps: true, versionKey: false }
);

citySchema.index({ location: '2dsphere' });

const City = mongoose.model('cities', citySchema, 'dcities');

City.getCity = async function (id) {
    const ifCity = await redis.get(id);
    if (ifCity) return ifCity;
    const city = await City.findById(id);
    await redis.set(id, city);
    return city;
};

City.refreshRedis = async function (id) {
    const city = await City.findById(id);
    await redis.set(id, city);
};

City.getCityFromLatLng = (lat, lng, status = constantUtils.ACTIVE) =>
    new Promise(async (res, rej) => {
        try {
            const city = await City.findOne({
                location: {
                    $geoIntersects: {
                        $geometry: {
                            type: 'Point',
                            coordinates: [lng, lat],
                        },
                    },
                },
                status,
            }).lean();
            res(city);
        } catch (err) {
            rej(err);
        }
    });

module.exports = City;
