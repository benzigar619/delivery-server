const mongoose = require('mongoose');

const constantUtils = require('../utils/constant.utils');

const activitySchema = new mongoose.Schema(
    {
        fromId: {
            type: mongoose.Schema.Types.ObjectId,
            required: true,
            refPath: 'fromType',
        },
        fromType: {
            type: String,
            required: true,
            enum: [
                constantUtils.ACTIVITIES.ADMINS,
                constantUtils.ACTIVITIES.LANGUAGES,
                constantUtils.ACTIVITIES.OTHERS,
                constantUtils.ACTIVITIES.SCREENS,
                constantUtils.ACTIVITIES.SHOPS,
                constantUtils.ACTIVITIES.USERS,
            ],
        },
        toId: {
            type: mongoose.Schema.Types.ObjectId,
            refPath: 'toType',
        },
        toType: {
            type: String,
            enum: [
                constantUtils.ACTIVITIES.ADMINS,
                constantUtils.ACTIVITIES.LANGUAGES,
                constantUtils.ACTIVITIES.OTHERS,
                constantUtils.ACTIVITIES.SCREENS,
                constantUtils.ACTIVITIES.USERS,
                constantUtils.ACTIVITIES.CITIES,
                constantUtils.ACTIVITIES.ZONES,
                constantUtils.ACTIVITIES.SHOPS,
                constantUtils.ACTIVITIES.CATALOGUES,
                constantUtils.ACTIVITIES.CATEGORIES,
                constantUtils.ACTIVITIES.PRODUCTS,
            ],
        },
        action: {
            type: String,
            required: true,
            // enum: [
            //     constantUtils.ACTIVITIES.UPDATE,
            //     constantUtils.ACTIVITIES.CREATE,
            //     constantUtils.ACTIVITIES.VIEW,
            //     constantUtils.ACTIVITIES.VIEW_USERS,
            //     constantUtils.ACTIVITIES.STATUS_CHANGE,
            //     constantUtils.ACTIVITIES.REGISTER_EMAIL_CHECK,
            //     constantUtils.ACTIVITIES.SEND_OTP,
            // ],
        },
        editKeys: {
            beforeEditing: {
                type: Object,
            },
            afterEditing: {
                type: Object,
            },
        },
        listType: {
            type: String,
        },
    },
    {
        versionKey: false,
        timestamps: true,
    }
);

const Activity = mongoose.model('activities', activitySchema, 'dactivities');

module.exports = Activity;
