/* NPM DEPENDENCIES */
const mongoose = require('mongoose');

const constantUtil = require('../utils/constant.utils');

const cityIdSchema = new mongoose.Schema(
    {
        cityId: {
            type: mongoose.Schema.Types.ObjectId,
            required: true,
            ref: 'cities',
            index: true,
        },
    },
    {
        _id: false,
    }
);

const categorySchema = new mongoose.Schema(
    {
        catalogueId: {
            type: mongoose.Schema.Types.ObjectId,
            optional: true,
            ref: 'catalogues',
        },
        categoryId: {
            type: mongoose.Schema.Types.ObjectId,
            optional: true,
            ref: 'categories',
        },
        categoryType: {
            type: String,
            required: true,
            enum: ['CATEGORY', 'SUBCATEGORY'],
        },
        cities: [cityIdSchema],
        name: {
            type: String,
            trim: true,
            default: '',
            index: true,
        },
        image: {
            type: String,
            trim: true,
        },
        secondaryImage: {
            type: String,
            trim: true,
        },
        status: {
            type: String,
            required: true,
            trim: true,
            enum: [constantUtil.ACTIVE, constantUtil.INACTIVE],
            default: constantUtil.ACTIVE,
            index: true,
        },
        lastEdited: {
            type: mongoose.Schema.Types.ObjectId,
            optional: true,
            ref: 'admins',
        },
        addedBy: {
            type: mongoose.Schema.Types.ObjectId,
            optional: true,
            ref: 'admins',
        },
        lastEditedTime: {
            type: Date,
            optional: true,
        },
        byPassSubcategory: {
            type: Boolean,
            optional: true,
        },
        subCategories: [
            {
                type: mongoose.Schema.Types.ObjectId,
                optional: true,
                ref: 'categories',
            },
        ],
        products: [
            {
                type: mongoose.Schema.Types.ObjectId,
                optional: true,
                ref: 'products',
            },
        ],
    },
    { timestamps: true, versionKey: false }
);

const categories = mongoose.model('categories', categorySchema, 'dcategories');

module.exports = categories;
