const _ = require('lodash');
const mongoose = require('mongoose');
const responseTime = require('response-time');
const handler = require('express-async-handler');

const Language = require('../models/language.model');
const Admin = require('../models/admin.models');
const Other = require('../models/other.model');
const Log = require('../models/log.model');
const City = require('../models/city.model');
const Rating = require('../models/rating.model');
const Partner = require('../models/partner.model');
const User = require('../models/user.model');
const Product = require('../models/product.model');

const redis = require('../utils/redis.utils');
const constantUtils = require('../utils/constant.utils');
const mailUtils = require('../utils/mail.utils');
const fcmUtils = require('../utils/fcm.utils');

const utils = {};

// Structures
utils.productStructure = (eachProduct) => ({
    id: eachProduct._id,
    name: eachProduct?.name,
    contentType: 'PRODUCT',
    image: eachProduct?.image,
    secondaryImage: eachProduct?.secondaryImage ?? '',
    maxCartLimit: eachProduct?.maxCartLimit ?? 20,
    price: eachProduct?.price,
    description: eachProduct?.description,
    vegNonVeg: eachProduct?.vegNonVeg,
    quantity: eachProduct?.quantity,
});

utils.concateAddress = (addresses = []) => {
    let address = '';
    addresses.forEach((eachAddress) => {
        if (eachAddress) address += eachAddress + ', ';
    });
    return address;
};

utils.orderStructure = (order) => ({
    id: order?._id,
    orderId: order?.order?.uniqueId,
    orderType: order?.order?.orderType,
    status: order?.order?.status,
    user: {
        id: order?.user?.id,
        phoneCode: order?.user?.phone?.code,
        phoneNumber: order?.user?.phone?.code,
    },
    invoice: order?.invoice?.final?.total
        ? order?.invoice?.final
        : order?.invoice?.estimation,
    address: order?.address?.deliveryAddress,
    payment: order?.payment?.option,
});

utils.isValidTimeZone = (tz) => {
    if (!Intl || !Intl.DateTimeFormat().resolvedOptions().timeZone) {
        throw new Error('Time zones are not available in this environment');
    }

    try {
        Intl.DateTimeFormat(undefined, { timeZone: tz });
        return true;
    } catch (ex) {
        return false;
    }
};

utils.UTCToTimeZone = (date = new Date(), timeZone = 'Asia/Kolkata') => {
    date = new Date(date);
    if (utils.isValidTimeZone(timeZone)) {
        const converted = new Date(
            date.toLocaleString('en-US', {
                timeZone: timeZone,
            })
        );
        return converted;
    } else return date;
};

utils.getDecimals = (number = 0, decimal = 2) => {
    if (number === undefined) return undefined;
    try {
        return parseFloat(number.toFixed(decimal));
    } catch (err) {
        return undefined;
    }
};

utils.getInvoiceValue = (invoice = {}, key = '') => {
    if (invoice?.final?.[key] !== undefined)
        return utils.getDecimals(invoice?.final?.[key]);
    else return utils.getDecimals(invoice?.estimation[key]);
};

utils.sendProductUpdatePartner = (partnerId, io = {}) => {
    // CANNOT BE IMPORTED IN THE START OF THE FILE
    const partnerProductStructure =
        require('../utils/structure.utils').partnerProductStructure;

    Partner.findById(partnerId)
        .populate('availableProducts.productId')
        .then((data) => {
            data?.deviceInfo?.map((each) =>
                io
                    ?.of(constantUtils.NS.PARTNERS)
                    ?.to(each?.deliverySocketId)
                    ?.emit(
                        constantUtils.SOCKET_EVENTS?.PRODUCTS_UPDATE,
                        data?.availableProducts
                            ?.filter(
                                (each) =>
                                    each?.productId?.status ===
                                    constantUtils.ACTIVE
                            )
                            ?.map((each) => partnerProductStructure(each))
                    )
            );
        });
};

utils.sendNotifications = (
    req,
    userType,
    action,
    data,
    userData,
    title = '',
    message = '',
    image = null
) => {
    try {
        const obj = {
            ty: constantUtils.SOCKET_EVENTS.NOTIFICATIONS, // type
            ut: userType, // userType
            a: action, // action
            ti: Date.now(), // timestamp
            m: message, // message
            d: data, // details
        };
        userData?.forEach((eachUser) => {
            eachUser?.deviceInfo?.forEach((each) => {
                // console.log(each);
                // Socket
                if (each?.deliverySocketId) {
                    req?.app?.io
                        ?.of?.(
                            userType === constantUtils.PARTNER
                                ? constantUtils.NS.PARTNERS
                                : constantUtils.NS.USERS
                        )
                        ?.to(each?.deliverySocketId)
                        ?.emit(each?.deviceId, obj);
                }
                // FCM
                if (each?.deviceId) {
                    fcmUtils.send({
                        userType,
                        platform: each?.platform,
                        to: each?.deviceId,
                        title,
                        body: message,
                        image,
                        data: {
                            data: obj,
                        },
                    });
                }
            });
        });
    } catch (err) {
        console.log(err);
        utils.errorLogging({}, err);
    }
};

utils.updateRating = (targetType = '', targetId = '', rating = 5) => {
    let Target = null;
    let condition = {};
    switch (targetType) {
        case constantUtils.USER:
            condition['userId'] = mongoose.Types.ObjectId(targetId);
            condition['ratingFor'] = constantUtils.USER;
            Target = User;
            break;
        case constantUtils.PARTNER:
            condition['partnerId'] = mongoose.Types.ObjectId(targetId);
            condition['ratingFor'] = constantUtils.PARTNER;
            Target = Partner;
            break;
        case constantUtils.PRODUCT:
            condition['productId'] = mongoose.Types.ObjectId(targetId);
            Target = Product;
            break;
        default:
            break;
    }

    if (!Target) return;

    try {
        // UPDATE RATING TEMPORARY CONCEPT
        Rating.aggregate([
            {
                $match: condition,
            },
            {
                $group: {
                    _id: '$rating',
                    total: { $sum: 1 },
                },
            },
            {
                $project: {
                    _id: 0,
                    rating: '$_id',
                    total: 1,
                },
            },
        ]).then((data) => {
            if (data?.length > 0) {
                let total = 0;
                let ratings = 0;
                let avg = 0;

                data?.forEach((each) => {
                    total += each?.total;
                    ratings += each?.total * each?.rating;
                });

                avg = ratings / total;

                Target.updateOne(
                    {
                        _id: targetId,
                    },
                    {
                        $set: {
                            'rating.ratingAverage': avg,
                            'rating.ratingInfo': data,
                            'rating.noOfOrdersRated': total,
                        },
                    }
                ).exec();
            }
        });
        // Rating.aggregate([
        //     {
        //         $match: condition,
        //     },
        //     {
        //         $group: {
        //             _id: '_id',
        //             total: { $sum: 1 },
        //             avg: { $avg: '$rating' },
        //         },
        //     },
        // ]).then((data) => {
        //     console.log(data);
        //     if (data?.length > 0)
        //         Target.updateOne(
        //             {
        //                 _id: targetId,
        //             },
        //             {
        //                 $set: {
        //                     'rating.ratingAverage': data[0]?.avg,
        //                     'rating.noOfOrdersRated': data[0]?.total,
        //                 },
        //             }
        //         ).exec();
        // });
    } catch (err) {
        utils.errorLogging({}, err);
    }
};

// Loggings
utils.apiLogging = responseTime(function (req, res, time) {
    if (
        mongoose.connection.readyState === 1 &&
        req.originalUrl.includes('logs') === false
    ) {
        Log.create({
            logType: 'API',
            data: {
                apiUrl: req.baseUrl + req.path,
                apiStatusCode: res.statusCode,
                apiPlatform: req?.headers?.['platform']?.toUpperCase() ?? 'WEB',
                apiSource: req.useragent.source,
                time: parseInt(time),
            },
        });
    }
});

utils.cronLogging = (cronType, process) => {
    if (mongoose.connection.readyState === 1) {
        Log.create({
            logType: 'CRON',
            data: {
                cronType,
                process,
            },
        });
    }
};

utils.errorLogging = async (req, err) => {
    if (mongoose.connection.readyState === 1) {
        const config = await Other.config();
        if (config?.sendError?.status) {
            mailUtils.sendMail(
                config?.sendError?.email ?? 'benzigar@berarkrays.com',
                'Code Error !!! ',
                `
                <h1>${req?.path}</h1>
                <p>${err.stack}</p>
                <p>body : ${JSON.stringify(req?.body, null, 2)}</p>
                <p>${new Date()}</p>
            `
            );
        }
        Log.create({
            logType: 'ERROR',
            data: {
                apiUrl: req.baseUrl + req.path,
                reqBody: req.body,
                message: err.stack,
            },
        });
    }
};

utils.clientErrorLogging = async (url, code, body, message, user = {}) => {
    if (mongoose.connection.readyState === 1) {
        Log.create({
            logType: 'CLIENT_ERROR',
            data: {
                errorCode: code,
                message: message,
                apiUrl: url,
                reqBody: body,
                userDetail: user,
            },
        });
    }
};

utils.validationLogging = async (url, type, body, err) => {
    if (mongoose.connection.readyState === 1) {
        Log.create({
            logType: 'VALIDATION',
            data: {
                validationType: type,
                validationMessage: err,
                // apiUrl: req.baseUrl + req.path,
                apiUrl: url,
                // reqBody: req.body,
                reqBody: body,
            },
        });
    }
};

utils.restartServerLog = (time) => {
    if (
        mongoose.connection.readyState === 1
        // && req.originalUrl.includes('logs') === false
    ) {
        Log.create({
            logType: 'RESTART_SERVER',
            data: {
                time,
            },
        });
    }
};

utils.consoleLog = (data, printConsole = true) => {
    if (printConsole) console.log(data);
    Log.create({
        logType: 'CONSOLE',
        data: {
            consoleData: data,
        },
    });
};

utils.differenceInObject = (coming, already) => {
    function changes(object, base) {
        return _.transform(object, function (result, value, key) {
            if (!_.isEqual(value, base[key])) {
                result[key] =
                    _.isObject(value) && _.isObject(base[key])
                        ? changes(value, base[key])
                        : value;
            }
        });
    }
    return changes(coming, already);
};

utils.sleep = (ms) => new Promise((resolve) => setTimeout(resolve, ms));

utils.getLanguageKey = async (KEY, langCode) => {
    let AllLanguages = [];
    // redis or mongodb
    AllLanguages = await redis.get(constantUtils.LANGUAGES);
    if (!AllLanguages) {
        AllLanguages = await Language.find();
        await utils.updateLanguageRedis();
    }
    const ifLangCode = AllLanguages.find(
        (each) => each.languageCode === langCode
    );
    if (ifLangCode === undefined) {
        const defaultLanguage = AllLanguages.find(
            (each) => each.languageDefault === true
        );
        return defaultLanguage && defaultLanguage.languageAdminKeys
            ? defaultLanguage?.languageAdminKeys[KEY] || ''
            : '';
    }
    return ifLangCode.languageAdminKeys[KEY] || '';
};

utils.getTranslation = (obj, key, langCode) => {
    let langObj = obj.translations.filter(
        (each) => each.languageCode === langCode
    )[0];

    if (langObj) return langObj[key];
    else return obj[key];
};

utils.updateLanguageRedis = async () => {
    const allLanguages = await Language.find();
    await redis.set(constantUtils.LANGUAGES, allLanguages);
};

utils.updateCityRedis = async () => {
    const allCity = await City.find().lean();
    allCity.forEach((each) => {
        if (each?._id) redis.set(each._id, each);
    });
};

utils.getHoursAndMinutes = (date) => {
    return {
        hour: new Date(date).getHours(),
        minutes: new Date(date).getMinutes(),
    };
};

utils.updateGeneralSettingsRedis = async () => {
    const general = await Other.findOne({
        name: constantUtils.GENERALSETTING,
    });
    await redis.set(constantUtils.GENERALSETTING, general);
};

utils.generateRandom = (length, chars) => {
    let mask = '';
    if (chars.indexOf('a') > -1) mask += 'abcdefghijklmnopqrstuvwxyz';
    if (chars.indexOf('A') > -1) mask += 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    if (chars.indexOf('#') > -1) mask += '0123456789';
    let result = '';
    for (let i = length; i > 0; --i)
        result += mask[Math.floor(Math.random() * mask.length)];
    return result;
};

utils.paginationParams = (req, res, next) => {
    req.page =
        req.query.page && req.query.page > 0 ? parseInt(req.query.page) : 1;
    req.count =
        req.query.count && req.query.count > 0 ? parseInt(req.query.count) : 10;
    next();
};

utils.generateSearch = (fields, search) => {
    let final = [];
    fields.map((each) => {
        let temp = {};
        temp[each] = {
            $regex: search || '',
            $options: 'i',
        };
        final.push(temp);
    });
    return final;
};

utils.toPascalCase = (string) => {
    return `${string}`
        .replace(new RegExp(/[-_]+/, 'g'), ' ')
        .replace(new RegExp(/[^\w\s]/, 'g'), '')
        .replace(
            new RegExp(/\s+(.)(\w*)/, 'g'),
            ($1, $2, $3) => `${$2.toUpperCase() + $3.toLowerCase()}`
        )
        .replace(new RegExp(/\w/), (s) => s.toUpperCase());
};

utils.ifFileAvailable = (req, image) =>
    !(
        req.body.avatar === undefined &&
        req.files.filter((each) => each.fieldname === image).length === 0
    );

utils.getSkip = (req) => (req.page - 1) * req.count;
utils.getLimit = (req) => req.count;
utils.getTotalPages = (req, count) =>
    Math.ceil(count / req.count) === 0 ? 1 : Math.ceil(count / req.count);
utils.getCurrentPage = (req) => req.page;

module.exports = utils;
