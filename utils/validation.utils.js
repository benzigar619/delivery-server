const Joi = require('joi');
const JoiOid = require('joi-oid');

const constantUtils = require('./constant.utils');

const validation = {};

validation.phoneCodeValidation = Joi.string().required().max(5).min(2);
validation.idValidation = JoiOid.objectId();
validation.otpValidation = Joi.string().min(1).required();
validation.phoneNumberValidation = Joi.number()
    .required()
    .min(999)
    .max(9999999999999999)
    .custom((value, helper) => value.toString());
validation.emailValidation = Joi.string().email().required();
validation.passwordValidation = Joi.string().required().max(20).min(6);
validation.optionalPasswordValidation = Joi.string().max(20).min(6);
validation.genderValidation = Joi.string()
    .valid(constantUtils.MALE, constantUtils.FEMALE, 'OTHERS')
    .required();
validation.nameValidation = Joi.string().required().min(1).max(300).trim();
validation.descriptionValidation = Joi.string().required().trim();
validation.pageValidation = Joi.number().min(0).default(0);
validation.docsPerPage = Joi.number().min(1).default(10);
validation.urlValidation = Joi.string().min(1).uri();
validation.statusValidation = Joi.string()
    .valid(constantUtils.ACTIVE, constantUtils.INACTIVE, constantUtils.ARCHIEVE)
    .default(constantUtils.ACTIVE);
validation.placeIdValidation = Joi.string().min(1).max(200).required();
validation.latLngValidation = Joi.number().required();
validation.latValidation = Joi.number().min(-90).max(90).required();
validation.lngValidation = Joi.number().min(-180).max(180).required();
validation.algorithmValidation = Joi.string()
    .valid(
        constantUtils.STOCK_PARTNER_ZONE_BASED_ALL_PARTNERS,
        constantUtils.STOCK_PARTNER_ZONE_BASED_ONE_BY_ONE_PARTNERS,
        constantUtils.STOCK_PARTNER_NEAR_BY_ALL_PARTNERS,
        constantUtils.STOCK_PARTNER_NEAR_BY_ONE_BY_ONE_PARTNERS
    )
    .required();

validation.ratingValidation = Joi.number().valid(1, 2, 3, 4, 5).required();

validation.cancellationValidation = Joi.object({
    status: Joi.boolean().required(),
    threshold: Joi.number().required(),
    feeStatus: Joi.boolean().required(),
    feeAmount: Joi.number().min(0).required(),
    feeAfterMinutes: Joi.number().min(0).required(),
});

validation.payOutValidation = Joi.string()
    .valid(constantUtils.INSTANT, constantUtils.BILLING)
    .required();
validation.paymentValidation = Joi.string()
    .valid(
        constantUtils.WALLET,
        constantUtils.CARD,
        constantUtils.CASH,
        constantUtils.CREDIT
    )
    .required();
validation.percentageValidation = Joi.number().min(0).max(100).required();
validation.platformValidation = Joi.string()
    .valid('WEB', 'ANDROID', 'IOS')
    .required();
validation.documentValidation = Joi.object().keys({
    documentName: validation.nameValidation,
    documents: Joi.array()
        .items(validation.urlValidation.required())
        .required(),
    expiryDate: Joi.date(),
});
module.exports = validation;
