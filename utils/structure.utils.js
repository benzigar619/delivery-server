const helperUtils = require('./helper.utils');
const constantUtils = require('./constant.utils');

const utils = {};

utils.mobileConfigStructure = (config) => ({
    deepLinkBaseUser: config?.deepLinkBaseUser,
    deepLinkBasePartner: config?.deepLinkBasePartner,
    shop: config?.shop,
    otpLength: config?.otpLength,
    otpRetrySeconds: config?.otpRetrySeconds,
    secureKey: config?.secureKey,
    darkLogo: config?.darkLogo,
    defaultDialCode: config?.defaultDialCode,
    favicon: config?.favicon,
    lightLogo: config?.lightLogo,
    emailAddress: config?.emailAddress,
    mapApi: config?.mapApi,
    pageViewLimits: config?.pageViewLimits,
    partner: config?.partner,
    partnerLocationUpdateInterval: config?.partnerLocationUpdateInterval,
    partnerOnOrderLocationUpdateInterval:
        config?.partnerOnOrderLocationUpdateInterval,
});

utils.productStructure = (eachProduct) => ({
    id: eachProduct._id,
    name: eachProduct?.name,
    contentType: 'PRODUCT',
    image: eachProduct?.image,
    secondaryImage: eachProduct?.secondaryImage ?? '',
    maxCartLimit: eachProduct?.maxCartLimit ?? 20,
    price: eachProduct?.price,
    description: eachProduct?.description,
    vegNonVeg: eachProduct?.vegNonVeg,
    quantity: eachProduct?.quantity,
    rating: eachProduct?.rating ? eachProduct?.rating : undefined,
});

utils.partnerProductStructure = (each) => ({
    id: each?.productId?._id,
    name: each?.productId?.name,
    price: each?.productId?.price,
    image: each?.productId?.image,
    secondaryImage: each?.productId?.secondaryImage,
    productType: each?.productId?.productType,
    description: each?.productId?.description,
    vegNonVeg: each?.productId?.vegNonVeg,
    totalStock: each?.maxStockQuantity ?? each?.availableStock,
    availableStock: each?.stockQuantity,
    productInCatalogueStatus: each?.productId?.status,
    lastReFillTime: each?.lastReFillTime,
    status: each?.status,
});

utils.orderStructure = (order) => ({
    id: order?._id,
    orderId: order?.order?.uniqueId,
    orderType: order?.order?.orderType,
    orderTime: order?.createdAt,
    otp:
        order?.order?.orderType === constantUtils.DESTINATION_ARRIVED ||
        order?.order?.orderType === constantUtils.DELIVERED
            ? order?.order?.otp
            : undefined,
    arriveRadius: order?.city?.algorithm?.arriveRadius,
    status: order?.order?.status,
    partner: order?.partner?.id?.firstName
        ? {
              id: order?.partner?.id?._id ?? order?.partner?.id,
              firstName: order?.partner?.id?.firstName,
              lastName: order?.partner?.id?.lastName,
              email: order?.partner?.id?.email,
              avatar: order?.partner?.id?.avatar,
              phoneCode: order?.partner?.phone?.code,
              phoneNumber: order?.partner?.phone?.number,
              rating: order?.partner?.rating?.ratingAverage,
          }
        : undefined,
    user: {
        id: order?.user?.id?._id ?? order?.user?.id,
        firstName: order?.user?.id?.firstName,
        lastName: order?.user?.id?.lastName,
        email: order?.user?.id?.email,
        avatar: order?.user?.id?.avatar,
        phoneCode: order?.user?.phone?.code,
        phoneNumber: order?.user?.phone?.number,
    },
    products: order?.product?.orderedProducts?.map((each) =>
        utils.productStructure(each)
    ),
    activity: order?.activity,
    invoice: {
        serviceTax: helperUtils.getInvoiceValue(order?.invoice, 'serviceTax'),
        deliveryCharge: helperUtils.getInvoiceValue(
            order?.invoice,
            'deliveryCharge'
        ),
        nightFare: helperUtils.getInvoiceValue(order?.invoice, 'nightFare'),
        packaging: helperUtils.getInvoiceValue(order?.invoice, 'packaging'),
        products: helperUtils.getInvoiceValue(order?.invoice, 'products'),
        total: helperUtils.getInvoiceValue(order?.invoice, 'total'),
    },
    address: order?.address?.deliveryAddress,
    payment: order?.payment?.option,
    rating: order?.order?.rating ?? undefined,
});

utils.partnerLoginStructure = (partner) => ({
    id: partner?._id,
    firstName: partner?.firstName,
    lastName: partner?.lastName,
    email: partner?.email,
    gender: partner?.gender,
    phone: {
        code: partner?.phone?.code,
        number: partner?.phone?.number,
    },
    avatar: partner?.avatar,
    accessToken: partner?.deviceInfo[0]?.accessToken,
    platform: partner?.deviceInfo[0]?.platform,
    languageCode: partner?.languageCode,
    emailVerified: partner?.emailVerificationTime ? true : false,
    onlineStatus:
        partner?.onlineStatus === true
            ? constantUtils.ONLINE
            : constantUtils.OFFLINE,
    onGoingOrders: partner?.onGoingOrders ?? [],
    reFillRequest: {
        status: partner?.reFillRequest?.status ?? constantUtils.CLOSED,
        lastRequestedTime: partner?.reFillRequest?.newTime,
        lastFilledTime: partner?.reFillRequest?.closedTime,
    },
    location: {
        lat: partner?.location?.coordinates[1] ?? 0,
        lng: partner?.location?.coordinates[0] ?? 0,
    },
    city: {
        id: partner?.cityId?._id ?? '',
        currencyCode: partner?.cityId?.currencyCode ?? '',
        currencySymbol: partner?.cityId?.currencySymbol ?? '',
        cityName: partner?.cityId?.locationName ?? '',
        requestInterval: partner?.cityId?.algorithm?.orderRequestTimeOut ?? 10,
        userCancellation: {
            status:
                partner?.cityId?.orderCalculation?.user?.cancellation?.status ??
                false,
            threshold:
                partner?.cityId?.orderCalculation?.user?.cancellation
                    ?.threshold ?? 0,
        },
        partnerCancellation: {
            status:
                partner?.cityId?.orderCalculation?.partner?.cancellation
                    ?.status ?? false,
            threshold:
                partner?.cityId?.orderCalculation?.partner?.cancellation
                    ?.threshold ?? 0,
        },
    },
    vehicle: {
        id: partner?.vehicles[0]?.vehicleId?._id ?? '',
        name: partner?.vehicles[0]?.vehicleId?.name ?? '',
        plateNumber: partner?.vehicles[0]?.vehicleId?.plateNumber ?? '',
        maker: partner?.vehicles[0]?.vehicleId?.maker ?? '',
        model: partner?.vehicles[0]?.vehicleId?.model ?? '',
    },
    zone: {
        id: partner?.zones[0]?.zoneId?._id ?? '',
        zoneName: partner?.zones[0]?.zoneId?.zoneName ?? '',
        location: partner?.zones[0]?.zoneId?.location?.coordinates[0] ?? [],
    },
});

utils.userHomeScreenAndCityStructure = (city, shop, homeScreen, langCode) => ({
    city: {
        id: city?._id,
        name: city?.locationName ?? '',
        currencySymbol: city?.currencySymbol ?? '',
        currencyCode: city?.currencyCode ?? '',
        timezone: city?.timezone ?? '',
        requestInterval: city?.algorithm?.orderRequestTimeOut ?? 10,
        retryCount: city?.algorithm?.orderRetryCount ?? 2,
        userCancellation: {
            status: city?.orderCalculation?.user?.cancellation?.status ?? false,
            threshold:
                city?.orderCalculation?.user?.cancellation?.threshold ?? 0,
        },
        partnerCancellation: {
            status:
                city?.orderCalculation?.partner?.cancellation?.status ?? false,
            threshold:
                city?.orderCalculation?.partner?.cancellation?.threshold ?? 0,
        },
    },
    shop: {
        shopName: shop?.shopName,
        email: shop?.email,
        phoneCode: shop?.phone?.code,
        phoneNumber: shop?.phone?.number,
        onlineStatus: shop?.onlineStatus ?? false,
        availableDays: shop?.serviceAvailableTime?.days,
        openTime: shop?.serviceAvailableTime?.startTime,
        closeTime: shop?.serviceAvailableTime?.endTime,
    },
    homeScreen: homeScreen.data.homeContents.map((each) => ({
        title: helperUtils.getTranslation(each, 'title', langCode),
        style: each?.style ?? '',
        type: each?.type ?? '',
        scroll: each?.scroll ?? '',
        image: each.image,
    })),
});

utils.userLoginStructure = (user) => ({
    firstName: user?.firstName,
    lastName: user?.lastName,
    email: user?.email,
    gender: user?.gender,
    phone: {
        code: user?.phone?.code,
        number: user?.phone?.number,
    },
    avatar: user?.avatar,
    accessToken: user?.deviceInfo[0]?.accessToken,
    languageCode: user?.languageCode,
    emailVerified: user?.emailVerificationTime ? true : false,
    onGoingOrders:
        user?.onGoingOrders?.map((each) => ({
            id: each?._id,
            status: each?.order?.status,
        })) ?? [],
});

utils.adminLoginStructure = (admin) => ({
    id: admin?._id,
    firstName: admin?.data?.firstName,
    userType: admin?.name,
    lastName: admin?.data?.lastName,
    email: admin?.data?.email,
    gender: admin?.data?.gender,
    phone: {
        code: admin?.data?.phone.code,
        number: admin?.data?.phone.number,
    },
    avatar: admin?.data?.avatar,
    accessToken: admin?.data?.accessToken,
    languageCode: admin?.data?.languageCode,
    preferredTheme: admin?.data?.preferredTheme ?? '',
});

utils.socketOrder = (order) => ({
    i: order?._id, //id
    t: order?.order?.orderType, // order type
    s: order?.order?.status, // status
    u: {
        // user
        id: order?.user?.id?._id, // id
        a: order?.user?.id?.avatar, // avatar
        fn: order?.user?.id?.firstName, // firstName
        ln: order?.user?.id?.lastName, // lastName
    },
    pa: order?.partner?.id?._id
        ? {
              // partner
              id: order?.partner?.id?._id, // id
              a: order?.partner?.id?.avatar, // avatar
              fn: order?.partner?.id?.firstName, // firstName
              ln: order?.partner?.id?.lastName, // lastName
          }
        : undefined,
    am: helperUtils.getInvoiceValue(order?.invoice, 'total'),
    pr:
        //products
        order?.product?.orderedProducts?.map((each) => ({
            n: each?.name?.substring(0, 20),
            q: each?.quantity,
            p: each?.price,
        })),
    a: {
        // address
        fa: order?.address?.deliveryAddress?.fullAddress,
        la: order?.address?.deliveryAddress?.lat ?? undefined,
        lg: order?.address?.deliveryAddress?.lng ?? undefined,
    },
    p: order?.payment?.option, // payment
});

module.exports = utils;
