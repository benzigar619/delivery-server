const config = require('../config');
const auth = require('../utils/auth.utils');
const redis = require('../utils/redis.utils');
const constantUtils = require('../utils/constant.utils');

const logs = require('../controllers/logs.controller');

const { format } = require('date-fns');

const adminSocket = require('../sockets/admin.socket');
const partnerSocketEvents = require('../sockets/partner.socket');
const userSocketEvents = require('../sockets/user.socket');

const utils = {};

let userConnections = [];
let notificationConnections = [];

utils.notificationSocket = {};
utils.adminSocket = {};
utils.userSocket = {};
utils.partnerSocket = {};
utils.shopSocket = {};

utils.defaultSocket = {};

utils.io = {};

utils.socketConsole = (socket, msg = '') => {
    console.log('SOCKET : ' + msg + format(new Date(), 'p') + ' ' + socket.id);
};

utils.socketConnection = async (
    socket,
    namespace = 'ADMIN',
    type = 'CONNECT'
) => {
    let redisKey = '';
    if (namespace === 'ADMIN') redisKey = constantUtils.ADMIN_CONNECTIONS;
    if (namespace === 'USER') redisKey = constantUtils.USER_CONNECTIONS;
    if (namespace === 'PARTNER') redisKey = constantUtils.PARTNER_CONNECTIONS;
    if (namespace === 'SHOP') redisKey = constantUtils.SHOP_CONNECTIONS;
    if (type === 'CONNECT') {
        const connections = await redis.get(redisKey);
        if (!connections) {
            redis.set(redisKey, []);
        }

        redis.set(redisKey, [...connections, socket.id]);
        logs.socketLog(socket.id, namespace, 'CONNECTED');
        console.log(
            `SOCKET : ${namespace} CONNECTED ${format(
                new Date(socket.handshake.time),
                'p'
            )} ${socket.id}`
        );
    } else {
        const connections = await redis.get(redisKey);
        if (connections)
            redis.set(
                redisKey,
                connections.filter((each) => each !== socket.id)
            );
        logs.socketLog(socket.id, namespace, 'DISCONNECTED');
        console.log(
            `SOCKET : ${namespace} DISCONNECTED ${format(
                new Date(socket.handshake.time),
                'p'
            )} ${socket.id}`
        );
    }
};

// const notificationSocketConnect = (ioConnect) => {
//     const io = ioConnect.of('/notifications');

//     utils.notificationSocket = io;

//     io.on('connection', function (socket) {
//         // Connect
//         if (socket.handshake.query.type === 'SERVER')
//             utils.socketConsole(
//                 socket,
//                 ++adminConnections + ' SERVER CONNECTED '
//             );
//         else
//             utils.socketConsole(
//                 socket,
//                 ++notificationConnections + ' NOTIFICATION CONNECTED '
//             );
//         socket.onAny((event, ...args) => {
//             utils.socketConsole(socket, `Client Emitted : ${event} `);
//         });

//         // Sockets
//         notificationSocket(socket);

//         // Disconnect
//         socket.on('disconnect', function () {
//             if (socket.handshake.query.type === 'SERVER')
//                 utils.socketConsole(
//                     socket,
//                     --adminConnections + ' SERVER DISCONNECTED '
//                 );
//             else
//                 utils.socketConsole(
//                     socket,
//                     --notificationConnections + ' NOTIFICATION DISCONNECTED '
//                 );
//         });
//     });
// };

const adminSocketConnect = (ioConnect) => {
    const io = ioConnect.of(constantUtils.NS.ADMINS);
    utils.adminSocket = io;

    io.on('connection', async function (socket) {
        const ifAdmin = await auth.adminSocket(
            socket?.id,
            socket?.handshake?.query?.accessToken
        );
        if (ifAdmin) {
            io.to(socket?.id).emit('connection', true);
            ioConnect
                .of(constantUtils.NS.ADMINS)
                .emit('CONNECTION_CHANGE', true);
            utils.socketConnection(socket, 'ADMIN', 'CONNECT');
            socket.onAny((event, ...args) => {
                utils.socketConsole(socket, `Client Emitted : ${event} `);
            });
            // Sockets
            adminSocket(socket);
            // Disconnect
            socket.on('disconnect', function () {
                ioConnect
                    .of(constantUtils.NS.ADMINS)
                    .emit('CONNECTION_CHANGE', true);
                auth.adminSocketDisconnect(socket?.id);
                utils.socketConnection(socket, 'ADMIN', 'DISCONNECTED');
            });
        } else {
            socket.disconnect();
            io.emit('DISCONNECT', true);
        }
    });
};

const shopSocketConnect = (ioConnect) => {
    const io = ioConnect.of(constantUtils.NS.SHOPS);
    utils.shopSocket = io;

    io.on('connection', async function (socket) {
        const ifShop = await auth.shopSocket(
            socket?.id,
            socket?.handshake?.query?.accessToken
        );
        if (ifShop) {
            io.to(socket?.id).emit('connection', true);
            utils.socketConnection(socket, 'SHOP', 'CONNECT');
            socket.onAny((event, ...args) => {
                utils.socketConsole(socket, `Client Emitted : ${event} `);
            });
            // Sockets
            adminSocket(socket);
            // Disconnect
            socket.on('disconnect', function () {
                auth.shopSocketDisconnect(socket?.id);
                utils.socketConnection(socket, 'SHOP', 'DISCONNECTED');
            });
        } else {
            socket.disconnect();
            io.emit('DISCONNECT', true);
        }
    });
};

const userSocketConnect = (ioConnect) => {
    const io = ioConnect.of(constantUtils.NS.USERS);
    utils.userSocket = io;

    io.on('connection', async function (socket) {
        const user = await auth.userSocket(
            socket?.id,
            socket?.handshake?.query?.accessToken,
            socket?.handshake?.query?.deviceId
        );
        if (user) {
            io.to(socket?.id).emit('connection', true);
            ioConnect
                .of(constantUtils.NS.ADMINS)
                .emit('CONNECTION_CHANGE', true);
            utils.socketConnection(socket, 'USER', 'CONNECT');
            socket.onAny((event, ...args) => {
                utils.socketConsole(socket, `Client Emitted : ${event} `);
            });
            // Sockets
            userSocketEvents(ioConnect, socket, user);
            // Disconnect
            socket.on('disconnect', function () {
                ioConnect
                    .of(constantUtils.NS.ADMINS)
                    .emit('CONNECTION_CHANGE', true);
                utils.socketConnection(socket, 'USER', 'DISCONNECTED');
            });
        } else {
            socket.disconnect();
            io.emit('DISCONNECT', true);
        }
    });
};

const partnerSocketConnect = (ioConnect) => {
    const io = ioConnect.of(constantUtils.NS.PARTNERS);
    utils.partnerSocket = io;

    io.on('connection', async function (socket) {
        const partner = await auth.partnerSocket(
            socket?.id,
            socket?.handshake?.query?.accessToken,
            socket?.handshake?.query?.deviceId
        );
        if (partner) {
            io.to(socket?.id).emit('connection', true);
            ioConnect
                .of(constantUtils.NS.PARTNERS)
                .emit('CONNECTION_CHANGE', true);
            utils.socketConnection(socket, 'PARTNER', 'CONNECT');
            socket.onAny((event, ...args) => {
                utils.socketConsole(socket, `Client Emitted : ${event} `);
            });
            // Sockets
            partnerSocketEvents(ioConnect, socket, partner);
            // Disconnect
            socket.on('disconnect', function () {
                ioConnect
                    .of(constantUtils.NS.PARTNERS)
                    .emit('CONNECTION_CHANGE', true);
                utils.socketConnection(socket, 'PARTNER', 'DISCONNECTED');
            });
        } else {
            socket.disconnect();
            io.emit('DISCONNECT', true);
        }
    });
};

utils.connectSocket = async (app, port) => {
    try {
        const io = require('socket.io')(port, {
            transports: ['websocket'],
        });
        console.log('Socket running in ' + port);
        const redisAdapter = require('socket.io-redis');

        utils.defaultSocket = io;
        app.io = io;

        // Socket with Redis
        io.adapter(redisAdapter(config.REDIS));
        await redis.set(constantUtils.ADMIN_CONNECTIONS, []);
        await redis.set(constantUtils.USER_CONNECTIONS, []);
        await redis.set(constantUtils.PARTNER_CONNECTIONS, []);
        await redis.set(constantUtils.SHOP_CONNECTIONS, []);

        // IO Connection
        adminSocketConnect(io);
        userSocketConnect(io);
        partnerSocketConnect(io);
        shopSocketConnect(io);

        utils.connectivityCheck();
    } catch (err) {
        console.log(err);
    }
};

utils.connectivityCheck = () => {
    const io = require('socket.io-client');

    // const adminSocket = io(`ws://localhost:${config.SOCKET_PORT}/admins`, {
    //     transports: ['websocket'],
    //     query: {
    //         accessToken: '',
    //         deviceId: '',
    //     },
    // });
};

module.exports = utils;
