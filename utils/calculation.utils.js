const controller = {};

const Product = require('../models/product.model');

const constantUtils = require('./constant.utils');
const helperUtils = require('./helper.utils');

controller.calculateTax = (totalAmount, city) =>
    new Promise((res, rej) => {
        try {
            let tax = 0;
            if (city.orderCalculation.serviceTaxPercentage)
                tax =
                    totalAmount *
                    (city.orderCalculation.serviceTaxPercentage / 100);
            res(tax);
        } catch (err) {
            rej(err);
        }
    });

controller.calculateProducts = (products, city) =>
    new Promise(async (resolve, reject) => {
        try {
            let returnObj = {};
            returnObj.totalProductAmount = 0;
            returnObj.totalQuantity = 0;
            returnObj.packaging = 0;
            returnObj.products = await Product.find({
                _id: { $in: products.map((each) => each.id) },
                status: constantUtils.ACTIVE,
                'cities.cityId': city._id,
            })
                .select(
                    '-rating -cities -statusLastEdited -lastEdited -addedBy -lastEditedTime'
                )
                .lean();

            returnObj.products.forEach((each, idx) => {
                const product = products.find(
                    (eachProduct) => each._id.toString() === eachProduct.id
                );
                returnObj.products[idx]['quantity'] = product.quantity;
                returnObj.packaging +=
                    city.orderCalculation.shop.packagePerProduct *
                    product.quantity;
                returnObj.totalProductAmount += each.price * product.quantity;
                returnObj.totalQuantity += product.quantity;
            });

            resolve(returnObj);
        } catch (err) {
            reject(err);
        }
    });

controller.calculateDeliveryCharge = (city = {}, time = new Date()) =>
    new Promise((res, rej) => {
        try {
            let deliveryCharge = 0;
            let nightFare = 0;
            // If Stock Partner
            if (city.algorithm.partnerType === constantUtils.STOCK_PARTNER) {
                deliveryCharge =
                    city.orderCalculation.partner.minimumDeliveryCharge;
            }
            // Peak Fare
            if (
                city.orderCalculation.partner.minimumDeliveryCharge &&
                city.orderCalculation.partner.nightFare.status &&
                city.orderCalculation.partner.nightFare.startTime &&
                city.orderCalculation.partner.nightFare.endTime
            ) {
                const startHours = helperUtils.getHoursAndMinutes(
                    city.orderCalculation.partner.nightFare.startTime
                );
                const endHours = helperUtils.getHoursAndMinutes(
                    city.orderCalculation.partner.nightFare.endTime
                );
                const startTime = new Date();
                startTime.setHours(startHours.hour);
                startTime.setMinutes(startHours.minutes);

                const endTime = new Date();
                endTime.setHours(endHours.hour);
                endTime.setMinutes(endHours.minutes);

                if (
                    time >= startTime &&
                    time <= endTime &&
                    city.orderCalculation.partner.nightFare.fareRatio
                ) {
                    // deliveryCharge =
                    //     deliveryCharge *
                    //     city.orderCalculation.partner.nightFare.fareRatio;
                    nightFare =
                        deliveryCharge /
                        city.orderCalculation.partner.nightFare.fareRatio;
                }
            }

            res({
                deliveryCharge,
                nightFare,
            });
        } catch (err) {
            rej(err);
        }
    });

controller.calculateTotal = (city, shop, products, time) => {
    let returnObj = {};

    returnObj.tax = 0;
    returnObj.packaging = 0;
    returnObj.totalQuantity = 0;
    returnObj.totalProductAmount = 0;
    returnObj.deliveryCharge = 0;

    if (city.algorithm.partnerType === constantUtils.STOCK_PARTNER) {
        returnObj.deliveryCharge =
            city.orderCalculation.partner.minimumDeliveryCharge;
    }
    return returnObj;
};

module.exports = controller;
