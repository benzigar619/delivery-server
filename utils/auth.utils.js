const CryptoJS = require('crypto-js');
const handler = require('express-async-handler');
const bcrypt = require('bcrypt');

const Admin = require('../models/admin.models');
const User = require('../models/user.model');
const Partner = require('../models/partner.model');
const Shop = require('../models/shop.model');
const constantUtils = require('../utils/constant.utils');
const helperUtils = require('../utils/helper.utils');

const utils = {};

utils.generatePassword = (password) =>
    bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);

utils.comparePassword = (password, passwordb) =>
    bcrypt.compareSync(password, passwordb);

utils.hashSecret = (password) =>
    CryptoJS.enc.Hex.stringify(CryptoJS.SHA256(password));

const secret = utils.hashSecret(
    'my-32-character-ultra-secure-and-ultra-long-secret'
);

utils.base64Encode = (rawString) => {
    const wordArray = CryptoJS.enc.Utf8.parse(rawString);
    return CryptoJS.enc.Base64.stringify(wordArray)
        .replace('=', '')
        .replace('=', '');
};

utils.jwtSign = (payload = {}) => {
    payload.createdAt = new Date().toISOString();
    const base64Payload = utils.base64Encode(JSON.stringify(payload));
    const base64Signature = utils.base64Encode(
        CryptoJS.HmacSHA256(base64Payload, secret)
    );
    return utils.base64Encode(`${base64Signature}:${base64Payload}`);
};

utils.base64Decode = (base64String) => {
    try {
        const wordArray = CryptoJS.enc.Base64.parse(base64String);
        return CryptoJS.enc.Utf8.stringify(wordArray);
    } catch (error) {
        return false;
    }
};

utils.decode = (_token) => {
    const token = utils.base64Decode(_token);
    if (!token) return false;
    const [signature, payload] = token.split(':').filter((d) => d.length > 0);
    const base64Signature = utils.base64Encode(
        CryptoJS.HmacSHA256(payload, secret)
    );

    if (signature === base64Signature) {
        const decoded = utils.base64Decode(payload);
        if (!decoded) return false;
        const _payload = JSON.parse(decoded);
        return _payload;
    } else return false;
};

utils.decodeToken = (token) => {
    if (!token) throw '401|Token Required';
    if (!token.includes('Bearer ') || token.split(' ').length === 1)
        throw '401|Invalid Token';
    if (token.split(' ').length === 1) throw '401|Invalid Token';
    token = token.split(' ')[1];
    const payload = utils.decode(token);
    if (payload) return payload;
    throw '401|Invalid Token';
};

utils.adminTokenVerify = handler(async (req, res, next) => {
    const _payload = utils.decodeToken(req.headers.authorization);
    const userData = await Admin.findOne({
        _id: _payload._id,
        'data.accessToken': req.headers.authorization.split(' ')[1],
    }).lean();
    if (!userData) throw '401|Invalid Token';
    req.user = userData;
    next();
});

utils.shopTokenVerify = handler(async (req, res, next) => {
    const _payload = utils.decodeToken(req.headers.authorization);
    const userData = await Shop.findOne({
        _id: _payload._id,
        accessToken: req.headers.authorization.split(' ')[1],
    }).lean();
    if (!userData) throw '401|Invalid Token';
    req.user = userData;

    next();
});

utils.userTokenVerify = handler(async (req, res, next) => {
    const _payload = utils.decodeToken(req.headers.authorization);
    const user = await User.findOne({
        _id: _payload._id,
        status: constantUtils.ACTIVE,
        'deviceInfo.accessToken': req?.headers?.authorization?.split(' ')[1],
    }).lean();
    if (!user) throw '401|Invalid Token';
    req.user = user;
    next();
});

utils.partnerTokenVerify = handler(async (req, res, next) => {
    const _payload = utils.decodeToken(req.headers.authorization);
    const partner = await Partner.findOne({
        _id: _payload._id,
        status: constantUtils.ACTIVE,
        'deviceInfo.accessToken': req?.headers?.authorization?.split(' ')[1],
    }).lean();
    if (!partner) throw '401|Invalid Token';
    req.user = partner;
    next();
});

utils.userSocket = async (socketId, token, deviceId) => {
    try {
        bearerToken = `Bearer ${token}`;
        const _payload = utils.decodeToken(bearerToken);
        const userData = await User.findOne({
            _id: _payload._id,
            'deviceInfo.accessToken': token,
        });
        if (!userData) return false;
        else {
            await User.findOneAndUpdate(
                {
                    'deviceInfo.accessToken': token,
                },
                {
                    $set: {
                        'deviceInfo.$.deliverySocketId': socketId,
                        'deviceInfo.$.deviceId': deviceId,
                    },
                }
            );
            return true;
        }
    } catch (err) {
        return false;
    }
};

utils.partnerSocket = async (socketId, token, deviceId) => {
    try {
        bearerToken = `Bearer ${token}`;
        const _payload = utils.decodeToken(bearerToken);
        const userData = await Partner.findOne({
            _id: _payload._id,
            'deviceInfo.accessToken': token,
        });
        if (!userData) return false;
        else {
            const partner = await Partner.findOneAndUpdate(
                {
                    'deviceInfo.accessToken': token,
                },
                {
                    $set: {
                        'deviceInfo.$.deliverySocketId': socketId,
                        'deviceInfo.$.deviceId': deviceId,
                    },
                }
            );
            return partner;
        }
    } catch (err) {
        return false;
    }
};

utils.adminSocket = async (socketId, token) => {
    try {
        token = `Bearer ${token}`;
        const _payload = utils.decodeToken(token);
        const userData = await Admin.findOne({
            _id: _payload._id,
            'data.accessToken': token.split(' ')[1],
        }).lean();
        if (!userData) return false;
        else {
            await Admin.findByIdAndUpdate(userData._id, {
                $set: {
                    'data.socketId': socketId,
                },
            });
            return true;
        }
    } catch (err) {
        return false;
    }
};

utils.shopSocket = async (socketId, token) => {
    try {
        token = `Bearer ${token}`;
        const _payload = utils.decodeToken(token);
        const userData = await Shop.findOne({
            _id: _payload._id,
            accessToken: token.split(' ')[1],
        }).lean();
        if (!userData) return false;
        else {
            await Shop.findByIdAndUpdate(userData._id, {
                $set: {
                    socketId: socketId,
                },
            });
            return true;
        }
    } catch (err) {
        return false;
    }
};

utils.developerVerify = handler(async (req, res, next) => {
    if (req?.user?.name === 'DEVELOPER') next();
    else throw '400|' + constantUtils.ONLY_FOR_DEVELOPERS;
});

utils.adminSocketDisconnect = async (socketId) => {
    try {
        await Admin.findOneAndUpdate(
            { 'data.socketId': socketId },
            { $set: { 'data.socketId': '' } }
        );
    } catch (err) {
        console.log(err);
    }
};

utils.shopSocketDisconnect = async (socketId) => {
    try {
        await Shop.findOneAndUpdate(
            { socketId: socketId },
            { $set: { socketId: '' } }
        );
    } catch (err) {
        console.log(err);
    }
};

module.exports = utils;
