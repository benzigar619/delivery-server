const aws = require('aws-sdk');
const Admin = require('../models/admin.models');
const Other = require('../models/other.model');

const utils = {};

utils.delete = (fileName) => {
    return new Promise(async (resolve, reject) => {
        const config = await Other.config();
        const spaces = config?.spaces || null;
        if (
            spaces &&
            spaces.spacesKey &&
            spaces.spacesSecret &&
            spaces.spacesEndpoint &&
            spaces.spacesBucketName &&
            spaces.spacesBaseUrl &&
            spaces.spacesObjectName
        ) {
            if (
                fileName.includes(
                    spaces.spacesBaseUrl + '/' + spaces.spacesObjectName
                )
            ) {
                fileName = fileName.replace(
                    spaces.spacesBaseUrl + '/' + spaces.spacesObjectName + '/',
                    ''
                );
                const spacesKey = spaces.spacesKey;
                const spacesSecret = spaces.spacesSecret;
                const spacesEndpoint = new aws.Endpoint(spaces.spacesEndpoint);

                const s3 = new aws.S3({
                    endpoint: spacesEndpoint,
                    accessKeyId: spacesKey,
                    secretAccessKey: spacesSecret,
                });

                const params = {
                    Bucket: spaces.spacesBucketName,
                    Key: spaces.spacesObjectName + '/' + fileName,
                };
                s3.deleteObject(params, function (err, data) {
                    if (err) {
                        console.log(err, err.stack);
                        resolve('');
                    } else {
                        resolve(
                            spaces.spacesBaseUrl +
                                '/' +
                                spaces.spacesObjectName +
                                '/' +
                                fileName
                        );
                    }
                });
            } else {
                resolve('');
            }
        } else {
            console.log('Spaces key missing !! ');
            resolve('');
        }
    });
};

utils.upload = (file, fileName) => {
    return new Promise(async (resolve, reject) => {
        const config = await Other.config();
        const spaces = config?.spaces || null;
        if (
            spaces &&
            spaces.spacesKey &&
            spaces.spacesSecret &&
            spaces.spacesEndpoint &&
            spaces.spacesBucketName &&
            spaces.spacesBaseUrl &&
            spaces.spacesObjectName
        ) {
            const spacesKey = spaces.spacesKey;
            const spacesSecret = spaces.spacesSecret;
            const spacesEndpoint = new aws.Endpoint(spaces.spacesEndpoint);

            const s3 = new aws.S3({
                endpoint: spacesEndpoint,
                accessKeyId: spacesKey,
                secretAccessKey: spacesSecret,
            });

            const fileContents = file;
            const params = {
                ACL: 'public-read',
                Bucket: spaces.spacesBucketName,
                Key: spaces.spacesObjectName + '/' + fileName,
                Body: fileContents,
            };
            s3.putObject(params, function (err, data) {
                if (err) {
                    console.log(err, err.stack);
                    resolve('');
                } else {
                    resolve(
                        spaces.spacesBaseUrl +
                            '/' +
                            spaces.spacesObjectName +
                            '/' +
                            fileName
                    );
                }
            });
        } else {
            console.log('Spaces key missing !! ');
            resolve('');
        }
    });
};

module.exports = utils;
