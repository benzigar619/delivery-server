const Joi = require('joi');
const JoiOid = require('joi-oid');

const constantUtils = require('../utils/constant.utils');
const validationUtils = require('../utils/validation.utils');
const validation = {};

// Router Validations

validation.getAllUsers = Joi.object().keys({
    search: Joi.string().allow(''),
    filters: Joi.object({
        status: Joi.string()
            .valid(
                '',
                constantUtils.ACTIVE,
                constantUtils.INACTIVE,
                constantUtils.ARCHIEVE
            )
            .default(''),
        gender: Joi.string()
            .valid('', constantUtils.MALE, constantUtils.FEMALE)
            .default(''),
        startDate: Joi.string().allow(''),
        endDate: Joi.string().allow(''),
        phoneNumberVerifiedUsers: Joi.string()
            .valid('', 'VERIFIED', 'UNVERIFIED')
            .allow(''),
    }).default({}),
});

validation.getAllPartners = Joi.object().keys({
    search: Joi.string().allow(''),
    filters: Joi.object({
        status: Joi.string()
            .valid(
                '',
                constantUtils.ACTIVE,
                constantUtils.INACTIVE,
                constantUtils.ARCHIEVE
            )
            .default(''),
        gender: Joi.string()
            .valid('', constantUtils.MALE, constantUtils.FEMALE)
            .default(''),
        startDate: Joi.string().allow(''),
        endDate: Joi.string().allow(''),
        reFillRequest: Joi.string().valid(
            '',
            constantUtils.NEW,
            constantUtils.ATTENDED,
            constantUtils.CLOSED
        ),
    }).default({}),
});

validation.addPartner = Joi.object().keys({
    partnerId: validationUtils.idValidation,
    cityId: validationUtils.idValidation.required(),
    zoneId: validationUtils.idValidation.required(),
    firstName: validationUtils.nameValidation,
    lastName: validationUtils.nameValidation,
    gender: validationUtils.genderValidation,
    email: validationUtils.emailValidation,
    phoneCode: validationUtils.phoneCodeValidation,
    dob: Joi.date().required(),
    phoneNumber: validationUtils.phoneNumberValidation,
    avatar: validationUtils.urlValidation.required(),
    partnerDocuments: Joi.array().items(validationUtils.documentValidation),
    partnerType: Joi.string()
        .valid(
            constantUtils.STOCK_PARTNER,
            constantUtils.SHOP_PARTNER,
            constantUtils.INDEPENDENT_PARTNER
        )
        .required(),
    catalogueId: validationUtils.idValidation,
    languageCode: validationUtils.nameValidation,
    password: validationUtils.passwordValidation,
    vehicle: Joi.object()
        .keys({
            vehicleId: validationUtils.idValidation,
            name: Joi.string().min(1).max(300).trim(),
            plateNumber: Joi.string(),
            maker: Joi.string(),
            model: Joi.string(),
            year: Joi.number(),
            color: Joi.string(),
            noOfDoors: Joi.number(),
            noOfSeats: Joi.number(),
            commonImage: validationUtils.urlValidation,
            frontImage: validationUtils.urlValidation,
            backImage: validationUtils.urlValidation,
            leftImage: validationUtils.urlValidation,
            rightImage: validationUtils.urlValidation,
            vehicleDocuments: Joi.array().items(
                validationUtils.documentValidation
            ),
        })
        .required(),
});

validation.emailPasswordLogin = Joi.object().keys({
    email: validationUtils.emailValidation,
    password: validationUtils.passwordValidation,
    deviceId: Joi.string().required(),
    platform: validationUtils.platformValidation,
});

validation.updateLocation = Joi.object().keys({
    lat: validationUtils.latValidation,
    lng: validationUtils.lngValidation,
});

validation.toggleOnlineOffline = Joi.object().keys({
    lat: validationUtils.latValidation,
    lng: validationUtils.lngValidation,
});

validation.changePassword = Joi.object().keys({
    oldPassword: validationUtils.passwordValidation,
    newPassword: validationUtils.passwordValidation,
});

validation.forgotPasswordEmailSubmit = Joi.object().keys({
    token: Joi.string().required(),
    password: validationUtils.passwordValidation,
});

validation.getProfile = Joi.object().keys({
    deviceId: Joi.string().required(),
    platform: validationUtils.platformValidation,
});

validation.availableProductStatusChange = Joi.object().keys({
    productId: validationUtils.idValidation.required(),
    status: Joi.string()
        .valid(constantUtils.ACTIVE, constantUtils.INACTIVE)
        .required(),
});

validation.getViewPartner = Joi.object().keys({
    id: validationUtils.idValidation.required(),
});

validation.addPartnerAvailableProduct = Joi.object().keys({
    id: validationUtils.idValidation,
    partnerId: validationUtils.idValidation.required(),
    productId: validationUtils.idValidation.required(),
    stock: Joi.number().min(0),
    status: Joi.string()
        .valid(constantUtils.ACTIVE, constantUtils.INACTIVE)
        .required(),
});

validation.partnerProductToggleStatus = Joi.object().keys({
    partnerId: validationUtils.idValidation.required(),
    productId: validationUtils.idValidation.required(),
    status: Joi.string()
        .valid(constantUtils.ACTIVE, constantUtils.INACTIVE)
        .required(),
});

validation.partnerProductAddStock = Joi.object().keys({
    partnerId: validationUtils.idValidation.required(),
    productId: validationUtils.idValidation.required(),
    // type: Joi.string().valid('ADD', 'SUB').required(),
    quantity: Joi.number().required(),
});

validation.reFillRequestStatusChange = Joi.object().keys({
    partnerId: validationUtils.idValidation.required(),
    status: Joi.string()
        .valid(constantUtils.ATTENDED, constantUtils.CLOSED)
        .required(),
});

validation.acceptOrder = Joi.object().keys({
    orderId: validationUtils.idValidation.required(),
});

validation.arriveOrder = Joi.object().keys({
    orderId: validationUtils.idValidation.required(),
});

validation.deliverOrder = Joi.object().keys({
    orderId: validationUtils.idValidation.required(),
});

validation.getOrderDetail = Joi.object().keys({
    orderId: validationUtils.idValidation.required(),
});

validation.startOrder = Joi.object().keys({
    orderId: validationUtils.idValidation.required(),
});

validation.cancelOrder = Joi.object().keys({
    orderId: validationUtils.idValidation.required(),
});

validation.getOrdersList = Joi.object().keys({
    orderType: Joi.string().valid('ALL', 'COMPLETED', 'ONGOING').required(),
});

validation.orderRequestedPartnerStatusChange = Joi.object().keys({
    orderId: validationUtils.idValidation.required(),
    status: Joi.string()
        .valid(constantUtils.DENIED, constantUtils.SEEN)
        .required(),
});

module.exports = validation;
