const Joi = require('joi');
const JoiOid = require('joi-oid');

const constantUtils = require('../utils/constant.utils');
const validationUtils = require('../utils/validation.utils');
const validation = {};

// Router Validations

validation.getAllShops = Joi.object().keys({
    search: Joi.string().allow(''),
    filters: Joi.object({
        shopType: Joi.string().valid('', constantUtils.RESTAURANTS).default(''),
        status: Joi.string()
            .valid(
                '',
                constantUtils.ACTIVE,
                constantUtils.INACTIVE,
                constantUtils.ARCHIEVE
            )
            .default(''),
        headType: Joi.string().allow('', 'INDEPENDENT', 'HEADS'),
        startDate: Joi.string().allow(''),
        endDate: Joi.string().allow(''),
    }).default({}),
});

validation.getAllHeads = Joi.object().keys({
    search: Joi.string().allow(''),
    filters: Joi.object({
        status: Joi.string()
            .valid(
                '',
                constantUtils.ACTIVE,
                constantUtils.INACTIVE,
                constantUtils.ARCHIEVE
            )
            .default(''),
        startDate: Joi.string().allow(''),
        endDate: Joi.string().allow(''),
    }).default({}),
});

validation.convertShopToHead = Joi.object().keys({
    name: validationUtils.nameValidation,
    email: validationUtils.emailValidation,
    phoneCode: validationUtils.phoneCodeValidation,
    phoneNumber: validationUtils.phoneNumberValidation,
    password: validationUtils.passwordValidation,
    shopId: validationUtils.idValidation.required(),
});

validation.addShopToHead = Joi.object().keys({
    shopId: validationUtils.idValidation.required(),
    headId: validationUtils.idValidation.required(),
});

validation.addShop = Joi.object().keys({
    id: validationUtils.idValidation,
    shopName: validationUtils.nameValidation,
    cityId: validationUtils.idValidation.required(),
    zoneId: validationUtils.idValidation.required(),
    email: validationUtils.emailValidation,
    phoneCode: validationUtils.phoneCodeValidation,
    phoneNumber: validationUtils.phoneNumberValidation,
    avatar: validationUtils.urlValidation.required(),
    languageCode: validationUtils.nameValidation,
    password: validationUtils.passwordValidation,
    catalogueType: Joi.string().allow('IMPORT', 'NEW').required(),
    catalogueId: validationUtils.idValidation,
    catalogueName: Joi.string(),
    onlineStatus: Joi.boolean().default(false),
    availableDays: Joi.array()
        .items(Joi.string().valid(...constantUtils.DAYS_IN_WEEK))
        .required(),
    startTime: Joi.date().required(),
    endTime: Joi.date().required(),
    headId: validationUtils.idValidation,
    address: Joi.object()
        .keys({
            placeId: validationUtils.placeIdValidation,
            addressName: validationUtils.nameValidation,
            fullAddress: validationUtils.nameValidation,
            shortAddress: validationUtils.nameValidation,
            lat: validationUtils.latLngValidation,
            lng: validationUtils.latLngValidation,
        })
        .required(),
});

validation.loginWithEmail = Joi.object().keys({
    email: validationUtils.emailValidation,
    password: validationUtils.passwordValidation,
});

validation.addCatalogue = Joi.object().keys({
    id: validationUtils.idValidation,
    name: validationUtils.nameValidation,
    shopId: validationUtils.idValidation.required(),
});

validation.addCategory = Joi.object().keys({
    id: validationUtils.idValidation,
    cities: Joi.array().items(validationUtils.idValidation).required(),
    name: validationUtils.nameValidation,
    catalogueId: validationUtils.idValidation.required(),
    contains: Joi.string(),
    image: validationUtils.urlValidation,
    secondaryImage: validationUtils.urlValidation,
});

validation.addSubcategory = Joi.object().keys({
    id: validationUtils.idValidation,
    name: validationUtils.nameValidation,
    cities: Joi.array().items(validationUtils.idValidation).required(),
    catalogueId: validationUtils.idValidation.required(),
    categoryId: validationUtils.idValidation.required(),
    image: validationUtils.urlValidation,
    secondaryImage: validationUtils.urlValidation,
});

validation.addProducts = Joi.object().keys({
    id: validationUtils.idValidation,
    name: validationUtils.nameValidation,
    tags: Joi.array().items(Joi.string()),
    cities: Joi.array().items(validationUtils.idValidation).required(),
    catalogueId: validationUtils.idValidation.required(),
    categoryId: validationUtils.idValidation.required(),
    subCategoryId: validationUtils.idValidation.required(),
    productType: Joi.string()
        .valid(constantUtils.PREPARATION, constantUtils.QUANTITY_STOCK)
        .required(),
    image: validationUtils.urlValidation,
    price: Joi.number().min(0).required(),
    secondaryImage: validationUtils.urlValidation,
    maxCartLimit: Joi.number().min(0).required(),
    description: Joi.string(),
    vegNonVeg: Joi.string(),
});

validation.getSingleShopSearch = Joi.object().keys({
    search: Joi.string().required(),
});

validation.getSingleShopDetails = Joi.object().keys({
    type: Joi.string()
        .valid('CATEGORIES', 'BEST_SELLER', 'MUST_TRY')
        .required(),
    cityId: validationUtils.idValidation.required(),
});

validation.statusValidation = Joi.object().keys({
    id: validationUtils.idValidation.required(),
    status: Joi.string().valid('ACTIVE', 'INACTIVE').required(),
});

validation.getHeadsCatalogues = Joi.object().keys({
    id: validationUtils.idValidation.required(),
});

module.exports = validation;
