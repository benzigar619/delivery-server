const Joi = require('joi');
const JoiOid = require('joi-oid');

const constantUtils = require('../utils/constant.utils');
const validationUtils = require('../utils/validation.utils');
const validation = {};

// Router Validations

validation.getAllOrders = Joi.object().keys({
    search: Joi.string().allow(''),
    filters: Joi.object({
        status: Joi.string()
            .valid(
                '',
                constantUtils.INITIATED,
                constantUtils.ACCEPTED,
                constantUtils.STARTED,
                constantUtils.EXPIRED,
                constantUtils.USER_DENIED,
                constantUtils.DELIVERED,
                constantUtils.DESTINATION_ARRIVED,
                constantUtils.PARTNER_CANCELLED
            )
            .default(''),
        partnerAssigned: Joi.boolean().required(),
        cityId: Joi.string().allow(''),
        zoneId: Joi.string().allow(''),
        userId: Joi.string().allow(''),
        partnerId: Joi.string().allow(''),
    }).default({}),
});

validation.orderStatusChangeFromAdmin = Joi.object().keys({
    orderId: validationUtils.idValidation.required(),
    status: Joi.string().required(),
});

validation.manualAssignPartner = Joi.object().keys({
    orderId: validationUtils.idValidation.required(),
    partnerId: validationUtils.idValidation.required(),
});

validation.getAllAssignablePartners = Joi.object().keys({
    orderId: validationUtils.idValidation.required(),
    zoneOrNearBy: Joi.string().required(),
    nearByRadius: Joi.number().min(0),
});

validation.getEagleView = Joi.object().keys({
    fromDate: Joi.date(),
    cityId: validationUtils.idValidation,
    zoneId: validationUtils.idValidation,
});

validation.partnerManualOrder = Joi.object().keys({
    shopId: validationUtils.idValidation.required(),
    paymentType: validationUtils.paymentValidation,
    products: Joi.array()
        .items(
            Joi.object().keys({
                id: validationUtils.idValidation.required(),
                quantity: Joi.number().min(1),
            })
        )
        .min(1)
        .required(),
    deliveryAddress: Joi.object()
        .keys({
            addressHouseNo: Joi.string().allow(''),
            addressType: Joi.string().allow(''),
            addressDirection: Joi.string().allow(''),
            shortAddress: Joi.string().allow(''),
            fullAddress: Joi.string().required(),
            lat: validationUtils.latValidation,
            lng: validationUtils.lngValidation,
        })
        .required(),
});

module.exports = validation;
