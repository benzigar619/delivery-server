const handler = require('express-async-handler');
const Activity = require('../models/activity.model');
const Other = require('../models/other.model');

const constantUtils = require('../utils/constant.utils');
const helperUtils = require('../utils/helper.utils');

const { dot } = require('dot-object');

const controller = {};

controller.allActivities = handler(async (req, res) => {
    const count = await Activity.count();
    const activites = await Activity.find()
        .populate(
            'fromId',
            'name shopName shopType firstName lastName avatar email phone data.firstName data.userType data.lastName data.email data.avatar data.phone'
        )
        .populate(
            'toId',
            'name categoryType shopName zoneName locationName name languageName firstName lastName avatar email phone data.firstName data.userType data.lastName data.email data.avatar data.phone'
        )
        .skip(helperUtils.getSkip(req))
        .limit(helperUtils.getLimit(req))
        .sort('-_id')
        .lean();
    return res.json({
        totalPages: helperUtils.getTotalPages(req, count),
        totalDocs: count,
        currentPage: helperUtils.getCurrentPage(req),
        data: activites.map((each) => ({
            ...each,
            editKeys: dot(each?.editKeys ?? {}),
        })),
    });
});

controller.getAllUserActivities = handler(async (req, res) => {
    const condition = {
        $or: [
            {
                toType: constantUtils.ACTIVITIES.USERS,
            },
            {
                fromType: constantUtils.ACTIVITIES.USERS,
            },
        ],
    };
    const count = await Activity.count(condition);
    const activities = await Activity.find(condition)
        .populate(
            'fromId',
            'name firstName lastName avatar email phone data.firstName data.lastName data.email data.avatar data.phone'
        )
        .populate('toId', 'firstName lastName avatar email phone')
        .skip(helperUtils.getSkip(req))
        .limit(helperUtils.getLimit(req))
        .sort('-createdAt')
        .lean();
    return res.json({
        totalPages: helperUtils.getTotalPages(req, count),
        totalDocs: count,
        currentPage: helperUtils.getCurrentPage(req),
        data: activities,
    });
});

controller.getSingleUserActivities = handler(async (req, res) => {
    const condition = {
        fromId: req.body.id,
        fromType: constantUtils.ACTIVITIES.USERS,
    };
    const count = await Activity.count(condition);
    const activities = await Activity.find(condition)
        .populate('fromId', 'firstName lastName avatar email phone')
        .populate('toId', 'firstName lastName avatar email phone')
        .skip(helperUtils.getSkip(req))
        .limit(helperUtils.getLimit(req))
        .sort('-createdAt')
        .lean();
    return res.json({
        totalPages: helperUtils.getTotalPages(req, count),
        totalDocs: count,
        currentPage: helperUtils.getCurrentPage(req),
        data: activities,
    });
});

controller.getGeneralActivites = handler(async (req, res) => {
    const condition = {
        action: constantUtils.ACTIVITIES.UPDATE_GENERAL_SETTINGS,
    };
    const count = await Activity.count(condition);
    const activities = await Activity.find(condition)
        .populate(
            'fromId',
            'data.firstName data.lastName data.avatar data.email data.phone'
        )
        .skip(helperUtils.getSkip(req))
        .limit(helperUtils.getLimit(req))
        .sort('-createdAt')
        .lean();
    return res.json({
        totalPages: helperUtils.getTotalPages(req, count),
        totalDocs: count,
        currentPage: helperUtils.getCurrentPage(req),
        data: activities.map((each) => ({
            ...each,
            editKeys: {
                beforeEditing: dot(each?.editKeys?.beforeEditing ?? {}),
                afterEditing: dot(each?.editKeys?.afterEditing ?? {}),
            },
        })),
    });
});

controller.getCityActivities = handler(async (req, res) => {
    const condition = {
        toId: req?.body?.id,
        action: constantUtils.ACTIVITIES.UPDATE,
        toType: constantUtils.ACTIVITIES.CITIES,
    };
    const count = await Activity.count(condition);
    const activities = await Activity.find(condition)
        .populate(
            'fromId',
            'data.firstName data.lastName data.avatar data.email data.phone'
        )
        .skip(helperUtils.getSkip(req))
        .limit(helperUtils.getLimit(req))
        .sort('-createdAt')
        .lean();
    return res.json({
        totalPages: helperUtils.getTotalPages(req, count),
        totalDocs: count,
        currentPage: helperUtils.getCurrentPage(req),
        data: activities.map((each) => ({
            ...each,
            editKeys: {
                beforeEditing: dot(each?.editKeys?.beforeEditing ?? {}),
                afterEditing: dot(each?.editKeys?.afterEditing ?? {}),
            },
        })),
    });
});

controller.maintainActivitiesLimit = async () => {
    const config = await Other.config();
    const limit = config?.maintainDocs?.activitiesDocsLimit ?? 1000;
    const count = await Activity.count();
    if (count - limit > 0) {
        const idToDelete = await Activity.find()
            .select('_id')
            .limit(count - limit);
        await Activity.deleteMany({
            _id: { $in: idToDelete },
        });
    }
};

module.exports = controller;
