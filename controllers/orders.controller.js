const handler = require('express-async-handler');
const subMinutes = require('date-fns/subMinutes');
const mongoose = require('mongoose');
const { v4: uuid } = require('uuid');

const User = require('../models/user.model');
const Other = require('../models/other.model');
const Activity = require('../models/activity.model');
const Language = require('../models/language.model.js');
const Product = require('../models/product.model');
const Partner = require('../models/partner.model');
const City = require('../models/city.model');
const Shop = require('../models/shop.model');
const Zone = require('../models/zone.model');
const Order = require('../models/order.model');

const turf = require('@turf/turf');

const auth = require('../utils/auth.utils');

const smsUtils = require('../utils/sms.utils');
const helper = require('../utils/helper.utils');
const mail = require('../utils/mail.utils');
const fileUtils = require('../utils/upload.utils');
const structureUtils = require('../utils/structure.utils');
const calculationUtils = require('../utils/calculation.utils');

const constantUtils = require('../utils/constant.utils');

const controller = {};

controller.getAllOrders = handler(async (req, res) => {
    let condition = {
        $or: helper.generateSearch(
            [
                'order.uniqueId',
                'user.phone.number',
                'partner.phone.number',
                'address.deliveryAddress.fullAddress',
            ],
            req.body.search
        ),
    };

    // Sort
    let sort = '-createdAt';

    // Filters
    if (req?.body?.filters?.status)
        condition['order.status'] = req.body.filters.status;
    if (req?.body?.filters?.partnerAssigned)
        condition['partner.id'] = { $exists: true };
    if (req?.body?.filters?.cityId)
        condition['city.id'] = req?.body?.filters?.cityId;
    if (req?.body?.filters?.zoneId)
        condition['zone.id'] = req?.body?.filters?.zoneId;

    if (req?.body?.filters?.userId)
        condition['user.id'] = req?.body?.filters?.userId;
    if (req?.body?.filters?.partnerId)
        condition['partner.id'] = req?.body?.filters?.partnerId;
    // if (req?.body?.filters?.gender)
    //     condition['gender'] = req.body.filters.gender;
    // if (req?.body?.filters?.startDate)
    //     condition['createdAt'] = {
    //         $gte: req?.body.filters?.startDate,
    //     };
    // if (req?.body?.filters?.endDate)
    //     condition['createdAt'] = {
    //         $lte: req?.body?.filters?.endDate,
    //     };
    // if (req?.body?.filters?.phoneNumberVerifiedUsers === 'VERIFIED')
    //     condition['phoneNumberVerifiedTime'] = {
    //         $exists: true,
    //     };
    // if (req?.body?.filters?.phoneNumberVerifiedUsers === 'UNVERIFIED')
    //     condition['phoneNumberVerifiedTime'] = {
    //         $exists: false,
    //     };

    const [count, orders] = await Promise.all([
        Order.count(condition),
        Order.find(condition)
            .select(
                'order user partner city.id partner invoice createdAt ratings'
            )
            .populate('city.id', 'locationName currencySymbol')
            .populate('user.id', 'firstName lastName email phone avatar')
            .populate('zone.id', 'zoneName')
            .sort(sort)
            .skip(helper.getSkip(req))
            .limit(helper.getLimit(req))
            .lean(),
    ]);

    // Activity.create({
    //     fromId: partner._id,
    //     fromType: constantUtils.ACTIVITIES.ADMINS,
    //     action: constantUtils.ACTIVITIES.VIEW_LIST,
    //     listType: constantUtils.USERS,
    // });

    return res.json({
        totalPages: helper.getTotalPages(req, count),
        totalDocs: count,
        currentPage: helper.getCurrentPage(req),
        data: orders,
    });
});

controller.orderExpireCron = async () => {
    const config = await Other.config();
    if (config?.order?.orderExpireThreshold) {
        const orders = await Order.find({
            'order.orderType': constantUtils.INSTANT,
            'order.status': constantUtils.INITIATED,
            createdAt: {
                $lte: new Date(
                    subMinutes(new Date(), config?.order?.orderExpireThreshold)
                ),
            },
        }).select('_id order');
        if (orders?.length > 0) {
            helper.consoleLog(
                {
                    title: 'Rides Expired',
                    data: orders?.map((each) => each?.order?.uniqueId),
                },
                false
            );
            await Order.updateMany(
                {
                    _id: orders?.map((each) => each._id),
                },
                {
                    $set: {
                        'order.status': constantUtils.EXPIRED,
                        'invoice.final.serviceTax': 0,
                        'invoice.final.total': 0,
                        'invoice.final.deliveryCharge': 0,
                        'invoice.final.nightFare': 0,
                        'invoice.final.packaging': 0,
                        'invoice.final.products': 0,
                        'activity.expired.time': new Date(),
                    },
                }
            );
        }
    }
};

controller.getOrderView = handler(async (req, res) => {
    const order = await Order.findById(req?.body?.id)
        .populate('user.id')
        .populate('city.id')
        .populate('shop.id')
        .populate('zone.id')
        .populate('vehicle.id')
        .populate('partner.id')
        .populate('partner.requestReceived.id');
    return res.json(order);
});

controller.orderStatusChangeFromAdmin = handler(async (req, res) => {
    let order = await Order.findOne({
        _id: req?.body?.orderId,
    })
        .populate('user.id')
        .populate('partner.id')
        .lean();
    if (!order) throw '400|' + constantUtils.INVALID_ID;

    let msg = 'ARRIVED ORDER';

    // CANCEL ORDER
    if (req?.body?.status === constantUtils.ADMIN_CANCELLED) {
        const updateData = {};

        const updatedProducts = order?.partner?.id?.availableProducts?.map(
            (each) => {
                const orderedProduct = order?.product?.orderedProducts?.find(
                    (eachProduct) =>
                        eachProduct?._id?.toString() ===
                        each?.productId?.toString()
                );
                if (orderedProduct && each?.stockQuantity) {
                    each['stockQuantity'] =
                        each['stockQuantity'] + orderedProduct.quantity;
                }
                return each;
            }
        );

        updateData['order.status'] = constantUtils.ADMIN_CANCELLED;
        updateData['activity.adminCancelled.time'] = new Date();
        updateData['activity.adminCancelled.adminId'] = req?.user?._id;
        updateData['activity.adminCancelled.location.lat'] = new Date();
        updateData['activity.adminCancelled.location.lng'] = new Date();
        updateData['invoice.final.serviceTax'] = 0;
        updateData['invoice.final.deliveryCharge'] = 0;
        updateData['invoice.final.packaging'] = 0;
        updateData['invoice.final.products'] = 0;
        updateData['invoice.final.cancellationCharge'] = 0;
        updateData['invoice.final.total'] = 0;

        order = await Order.findByIdAndUpdate(
            req?.body?.orderId,
            {
                $set: updateData,
            },
            {
                new: true,
            }
        )
            .populate('user.id')
            .populate('partner.id');

        await Partner.updateOne(
            {
                _id: order?.partner?.id?._id,
            },
            {
                $set: {
                    availableProducts: updatedProducts,
                },
                $pull: {
                    onGoingOrders: {
                        orderId: order?._id,
                    },
                },
            },
            {
                new: true,
            }
        );

        // Updating Partner Products
        helper.sendProductUpdatePartner(order?.partner?.id?._id, req?.app?.io);

        msg = 'CANCELLED ORDER';
    }

    // STARTED
    if (
        req?.body?.status === constantUtils.STARTED &&
        order?.order?.status === constantUtils.ACCEPTED
    ) {
        const ifStartedOrder = await Order.count({
            'partner.id': order?.partner?.id?._id,
            'order.status': constantUtils.STARTED,
        });

        // If Partner Has Any Started Orders
        if (ifStartedOrder) {
            await Order.updateMany(
                {
                    'partner.id': order?.partner?.id?._id,
                    'order.status': constantUtils.STARTED,
                },
                {
                    $set: {
                        'order.status': constantUtils.ACCEPTED,
                        'switchedBack.adminId': req?.user?._id,
                        'switchedBack.time': new Date(),
                        'switchedBack.location.lat':
                            order?.partner?.id?.location?.coordinates[1],
                        'switchedBack.location.lng':
                            order?.partner?.id?.location?.coordinates[0],
                    },
                }
            );
            await Partner.updateMany(
                {
                    _id: order?.partner?.id?._id,
                    'onGoingOrders.status': constantUtils.STARTED,
                },
                {
                    $set: {
                        'onGoingOrders.$.status': constantUtils.ACCEPTED,
                    },
                }
            );
        }

        // UPDATING ORDER
        const updateData = {};
        updateData['order.status'] = constantUtils.STARTED;
        updateData['activity.started.time'] = new Date();
        updateData['activity.started.adminId'] = req?.user?._id;
        updateData['activity.started.location.lat'] =
            order?.partner?.id?.location?.coordinates[1];
        updateData['activity.started.location.lng'] =
            order?.partner?.id?.location?.coordinates[1];
        order = await Order.findByIdAndUpdate(
            {
                _id: req?.body?.orderId,
                'order.status': constantUtils.ACCEPTED,
                'partner.id': order?.partner?.id?._id,
            },
            {
                $set: updateData,
            },
            {
                new: true,
            }
        )
            .populate('user.id')
            .populate('partner.id');
        await Partner.updateOne(
            {
                _id: order?.partner?.id?._id,
                'onGoingOrders.orderId': req?.body?.orderId,
            },
            {
                $set: {
                    'onGoingOrders.$.status': constantUtils.STARTED,
                },
            },
            {
                new: true,
            }
        );

        msg = 'STARTED ORDER';
    }

    // ARRIVE
    if (
        req?.body?.status === constantUtils.DESTINATION_ARRIVED &&
        order?.order?.status === constantUtils.STARTED
    ) {
        // UPDATING PROFESSIONAL
        await Partner.updateOne(
            {
                _id: order?.partner?.id?._id,
                'onGoingOrders.orderId': req?.body?.orderId,
            },
            {
                $set: {
                    'onGoingOrders.$.status': constantUtils.DESTINATION_ARRIVED,
                },
            }
        );
        // UPDATING ORDER
        order = await Order.findByIdAndUpdate(
            order?._id,
            {
                $set: {
                    'order.status': constantUtils.DESTINATION_ARRIVED,
                    'activity.destinationArrived.time': new Date(),
                    'activity.destinationArrived.adminId': req?.user?._id,
                    'activity.destinationArrived.location.lat':
                        order?.partner?.id?.location?.coordinates[1],
                    'activity.destinationArrived.location.lng':
                        order?.partner?.id?.location?.coordinates[0],
                },
            },
            {
                new: true,
            }
        )
            .populate('user.id')
            .populate('partner.id');
        msg = 'ARRIVED ORDER';
    }

    // DELIVERED
    if (
        req?.body?.status === constantUtils.DELIVERED &&
        order?.order?.status === constantUtils.DESTINATION_ARRIVED
    ) {
        // UPDATING PARTER
        await Partner.findByIdAndUpdate(
            order?.partner?.id?._id,
            {
                $pull: {
                    onGoingOrders: {
                        orderId: order?._id,
                        status: constantUtils.DESTINATION_ARRIVED,
                    },
                },
            },
            {
                new: true,
            }
        );

        // UPDATING ORDERS
        order = await Order.findByIdAndUpdate(
            order?._id,
            {
                $set: {
                    'order.status': constantUtils.DELIVERED,
                    'activity.delivered.time': new Date(),
                    'activity.delivered.adminId': req?.user?._id,
                    'activity.delivered.location.lat':
                        order?.partner?.id?.location?.coordinates[1],
                    'activity.delivered.location.lng':
                        order?.partner?.id?.location?.coordinates[0],
                    'invoice.final': order?.invoice?.estimation,
                },
            },
            {
                new: true,
            }
        )
            .populate('user.id')
            .populate('partner.id');
        msg = 'DELIVERED ORDER';
    }

    // Notifications
    if (order?.user?.id?._id)
        helper.sendNotifications(
            req,
            constantUtils.USER,
            constantUtils.SOCKET_EVENTS.ORDER_STATUS_CHANGE,
            structureUtils.socketOrder(order),
            [order?.user?.id],
            '',
            msg
        );
    if (order?.partner?.id?._id)
        helper.sendNotifications(
            req,
            constantUtils.PARTNER,
            constantUtils.SOCKET_EVENTS.ORDER_STATUS_CHANGE,
            structureUtils.socketOrder(order),
            [order?.partner?.id],
            '',
            msg
        );
    return res.json({
        message: 'SUCCESS',
    });
});

controller.manualAssignPartner = handler(async (req, res) => {
    let [order, partner] = await Promise.all([
        Order.findOne({
            _id: req?.body?.orderId,
            'order.status': constantUtils.INITIATED,
            'partner.id': { $exists: false },
        }).lean(),
        Partner.findById(req?.body?.partnerId),
    ]);

    if (!order) throw '400|' + constantUtils.ORDER_ALREADY_ACCEPTED;
    if (!partner) throw '400|' + constantUtils.INVALID_PARTNER_ID;

    if (partner.onlineStatus === false)
        throw '400|' + constantUtils.PARTNER_OFFLINE;

    // Checking Maximum Orders
    if (
        (order?.city?.algorithm?.maxOrdersAtTime ?? 1) <=
        (partner?.onGoingOrders?.length ?? 0)
    )
        throw '400|' + constantUtils.MAX_ORDERS_REACHED;

    // CHECKING AND UPDATING PARTNER PRODUCTS
    order?.product?.orderedProducts?.forEach((each) => {
        const index = partner?.availableProducts?.findIndex(
            (eachProduct) =>
                eachProduct?.productId?.toString() === each?._id?.toString() &&
                eachProduct?.status === constantUtils.ACTIVE
        );
        if (index === -1) throw '400|' + constantUtils.PRODUCT_NOT_AVAILABLE;
        if (
            each?.productType === constantUtils.QUANTITY_STOCK &&
            partner?.availableProducts?.[index]?.stockQuantity < each?.quantity
        )
            throw '400|' + constantUtils.NOT_ENOUGH_STOCK_QUANTITY;
        else {
            if (each.productType === constantUtils.QUANTITY_STOCK)
                partner.availableProducts[index].stockQuantity =
                    partner?.availableProducts?.[index]?.stockQuantity -
                    each?.quantity;
        }
    });

    // UPDATING ORDER
    const updateData = {};
    updateData['partner.id'] = partner?._id;
    updateData['partner.phone.code'] = partner?.phone?.code;
    updateData['partner.phone.number'] = partner?.phone?.number;
    updateData['activity.accepted.time'] = new Date();
    updateData['activity.accepted.adminId'] = req?.user?._id;
    updateData['activity.accepted.location.lat'] =
        partner?.location?.coordinates[1];
    updateData['activity.accepted.location.lng'] =
        partner?.location?.coordinates[0];
    if (partner?.onGoingOrders?.length) {
        updateData['order.status'] = constantUtils.ACCEPTED;
    } else {
        updateData['order.status'] = constantUtils.STARTED;
        updateData['activity.started.time'] = new Date();
        updateData['activity.started.adminId'] = req?.user?._id;
        updateData['activity.started.location.lat'] =
            partner?.location?.coordinates[1];
        updateData['activity.started.location.lng'] =
            partner?.location?.coordinates[0];
    }
    order = await Order.findByIdAndUpdate(
        {
            _id: req?.body?.orderId,
            'order.status': constantUtils.INITIATED,
            'partner.id': { $exists: false },
        },
        {
            $set: updateData,
        },
        {
            new: true,
        }
    ).populate('user.id');

    if (!order) throw '400|' + constantUtils.ORDER_ALREADY_ACCEPTED;

    await Partner.findByIdAndUpdate(partner?._id, {
        $set: {
            availableProducts: partner.availableProducts,
        },
        $push: {
            onGoingOrders: {
                orderId: order?._id,
                status: order?.order?.status,
                acceptedTime: new Date(),
            },
        },
    });

    // NOTIFICATION
    if (order?.user?.id?._id)
        helper.sendNotifications(
            req,
            constantUtils.USER,
            constantUtils.SOCKET_EVENTS.ORDER_STATUS_CHANGE,
            structureUtils.socketOrder(order),
            [order?.user?.id],
            '',
            'ACCEPTED ORDER'
        );

    if (partner)
        helper.sendNotifications(
            req,
            constantUtils.PARTNER,
            constantUtils.SOCKET_EVENTS.MANUAL_ASSIGN_ORDER,
            structureUtils.socketOrder(order),
            [partner],
            '',
            'MANUAL ASSIGN ORDER'
        );

    return res.json(order);
});

controller.getAllAssignablePartners = handler(async (req, res) => {
    let order = await Order.findById(req?.body?.orderId)
        .populate('city.id')
        .populate('user.id')
        .populate('zone.id');

    if (!order) throw '400|' + constantUtils.INVALID_ID;

    if (order?.order?.status !== constantUtils.INITIATED)
        throw '400|' + constantUtils.ORDER_ALREADY_ACCEPTED;

    // Getting Partner Info
    let partners = [];
    let partnerCondition = {};
    partnerCondition['$and'] = [];

    const singleCondition = {
        onlineStatus: true,
        partnerType: constantUtils.STOCK_PARTNER,
        status: constantUtils.ACTIVE,
    };

    if (order?.city?.id?.algorithm?.maxOrdersAtTime) {
        singleCondition[
            `onGoingOrders.${order?.city?.id?.algorithm?.maxOrdersAtTime - 1}`
        ] = {
            $exists: false,
        };
    }
    partnerCondition['$and'].push(singleCondition);
    order?.product?.orderedProducts?.forEach((each) => {
        partnerCondition['$and'].push({
            availableProducts: {
                $elemMatch: {
                    productId: each?._id,
                    status: 'ACTIVE',
                    stockQuantity:
                        each?.productType === constantUtils.QUANTITY_STOCK
                            ? {
                                  $gte: each.quantity,
                              }
                            : undefined,
                },
            },
        });
    });

    // If Delivery Location in placed inside Zone
    if (
        req?.body?.zoneOrNearBy === constantUtils.ZONES &&
        order?.zone?.id?._id
    ) {
        partnerCondition['$and'].push({
            'zones.zoneId': order?.zone?.id?._id,
            location: {
                $geoWithin: {
                    $geometry: order?.zone?.id?.location,
                },
            },
        });
        partners = await Partner.find(partnerCondition).lean();
        if (partners.length === 0)
            throw '400|' + constantUtils.NO_PARTNERS_AVAILABLE;
    } else {
        partners = await Partner.aggregate([
            {
                $geoNear: {
                    near: {
                        type: 'Point',
                        coordinates: [
                            order?.address?.deliveryAddress.lng,
                            order?.address?.deliveryAddress.lat,
                        ],
                    },
                    distanceField: 'dist.calculated',
                    maxDistance:
                        req?.body?.nearByRadius ??
                        order?.city?.id?.algorithm?.region?.nearByRadius ??
                        0,
                    spherical: true,
                },
            },
            { $match: partnerCondition },
        ]);
        if (partners.length === 0)
            throw '400|' + constantUtils.NO_PARTNERS_AVAILABLE;
    }

    return res.json(partners);
});

controller.getEagleView = handler(async (req, res) => {
    let date = '';
    const condition = {};

    // DATE FILTER
    if (req?.body?.fromDate) date = new Date(req?.body?.fromDate);
    else date = subMinutes(new Date(), 180);

    let activityCondition = [];

    const activityTime = [
        'activity.initiated.time',
        'activity.accepted.time',
        'activity.started.time',
        'activity.destinationArrived.time',
        'activity.delivered.time',
        'activity.expired.time',
        'activity.userDenied.time',
        'activity.partnerCancelled.time',
        'activity.userCancelled.time',
        'activity.adminCancelled.time',
    ];

    activityTime?.map((each) => {
        let condition = {};
        condition[each] = {
            $gt: new Date(req?.body?.fromDate),
        };
        activityCondition.push(condition);
    });

    condition['$or'] = activityCondition;

    // condition['order.status'] = {
    //     $in: [
    //         constantUtils.INITIATED,
    //         constantUtils.ACCEPTED,
    //         constantUtils.STARTED,
    //         constantUtils.DESTINATION_ARRIVED,
    //         constantUtils.DELIVERED,
    //         constantUtils.USER_DENIED,
    //         constantUtils.ADMIN_CANCELLED,
    //         constantUtils.USER_CANCELLED,
    //         constantUtils.PARTNER_CANCELLED,
    //     ],
    // };

    if (req?.body?.cityId)
        condition['city.id'] = mongoose.Types.ObjectId(req?.body?.cityId);
    if (req?.body?.zoneId)
        condition['zone.id'] = mongoose.Types.ObjectId(req?.body?.zoneId);

    const orders = await Order.aggregate([
        {
            $match: condition,
        },
        {
            $sort: {
                _id: -1,
            },
        },
        {
            $project: {
                order: 1,
                user: 1,
                partner: 1,
                invoice: 1,
                'city.id': 1,
                zone: 1,
                address: 1,
                payment: 1,
                ratings: 1,
                createdAt: 1,
            },
        },
    ]);
    return res.json({
        total: orders?.length,
        data: orders,
    });
});

controller.partnerManualOrder = handler(async (req, res) => {
    // const order =  await Order.create({
    // })
    return res.json({
        message: 'Working',
    });
});

module.exports = controller;
