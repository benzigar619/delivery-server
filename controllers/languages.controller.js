const handler = require('express-async-handler');

const Language = require('../models/language.model');
const Other = require('../models/other.model');
const Activity = require('../models/activity.model');

const helperUtils = require('../utils/helper.utils');
const constantUtils = require('../utils/constant.utils');
const jobUtils = require('../utils/jobs.utils');

const controller = {};

controller.getAllLanguages = handler(async (req, res) => {
    const condition = {};
    const count = await Language.count(condition);

    const languages = await Language.find(
        condition,
        '-languageAdminKeys -createdAt -updatedAt'
    )
        .populate('addedBy')
        .sort('-createdAt')
        .skip(helperUtils.getSkip(req))
        .limit(helperUtils.getLimit(req));

    Activity.create({
        fromId: req.user._id,
        fromType: constantUtils.ACTIVITIES.ADMINS,
        action: constantUtils.ACTIVITIES.VIEW_LIST,
        listType: constantUtils.LANGUAGES,
    });
    return res.json({
        totalPages: helperUtils.getTotalPages(req, count),
        currentPage: req.page,
        data: languages.map((each) => ({
            id: each._id,
            languageName: each?.languageName ? each?.languageName : '',
            languageNativeName: each.languageNativeName,
            languageCode: each.languageCode,
            languageDirection: each.languageDirection,
            languageDefault: each.languageDefault,
            addedBy:
                (each?.addedBy?.data?.firstName ?? '') +
                ' ' +
                (each?.addedBy?.data?.lastName ?? ''),
            addedImage: each?.addedBy?.data?.avatar ?? '',
        })),
    });
});

controller.getAllLanguageNames = handler(async (req, res) => {
    const allLanguages = await Language.find().select(
        '-languageAdminKeys -createdAt -updatedAt'
    );
    return res.json(
        allLanguages.map((each) => ({
            id: each._id,
            languageName: each?.languageName ? each?.languageName : '',
            languageDirection: each?.languageDirection,
            languageCode: each?.languageCode,
            languageNativeName: each?.languageNativeName,
        }))
    );
});

controller.addLanguage = handler(async (req, res) => {
    const updateDoc = {
        languageName: req.body.languageName,
        languageCode: req.body.languageCode,
        languageNativeName: req.body.languageNativeName,
        languageDirection: req.body.languageDirection,
        languageMobileUserKeys: req.body.languageMobileUserKeys,
        languageMobilePartnerKeys: req.body.languageMobilePartnerKeys,
        languageAdminKeys: req.body.languageAdminKeys,
        lastEdited: req.user._id,
        languageDefault: req.body.languageDefault,
        lastEditedTime: new Date(),
    };
    let language = {};
    if (req.body.languageDefault === true) {
        await Language.updateMany(
            {},
            {
                $set: {
                    languageDefault: false,
                },
            }
        );
    }
    // For Updates
    if (req.body.id) {
        const ifLanguage = await Language.findOne({
            _id: { $ne: req.body.id },
            languageCode: req.body.languageCode,
        });
        if (ifLanguage) throw '400|' + constantUtils.LANGUAGE_CODE_EXISTS;
        language = await Language.findByIdAndUpdate(
            req.body.id,
            {
                $set: updateDoc,
            },
            { new: true }
        );
        Activity.create({
            fromId: req.user._id,
            fromType: constantUtils.ACTIVITIES.ADMINS,
            toId: req.body.id,
            toType: constantUtils.ACTIVITIES.LANGUAGES,
            action: constantUtils.ACTIVITIES.UPDATE,
        });
    }
    // New Record
    else {
        const ifLanguage = await Language.findOne({
            languageCode: req.body.languageCode,
        });
        if (ifLanguage) throw '400|' + constantUtils.LANGUAGE_CODE_EXISTS;
        else {
            const config = await Other.config();
            const concurrentLanguageJobs =
                config?.scrapping?.translateConcurrentJobs ?? 2;
            const activeJobs = jobUtils.getActiveLanguageTranslateJobs();
            if (concurrentLanguageJobs < activeJobs.length)
                throw '400|' + constantUtils.MAX_JOBS_REACHED;
            language = await Language.create({
                ...updateDoc,
                addedBy: req.user._id,
            });
            Activity.create({
                fromId: req.user._id,
                fromType: constantUtils.ACTIVITIES.ADMINS,
                toId: language.id,
                toType: constantUtils.ACTIVITIES.LANGUAGES,
                action: constantUtils.ACTIVITIES.ADD,
            });
            if (req.body.autoGenerateKeys) {
                jobUtils.translateFromBingScrapping(
                    req.body.languageAdminKeys,
                    language.languageCode,
                    language._id
                );
            }
        }
    }
    helperUtils.updateLanguageRedis();
    return res.json(language);
});

controller.getViewLanguage = handler(async (req, res) => {
    const language = await Language.findOne({
        _id: req.body.id,
    })
        .populate(
            'lastEdited addedBy',
            'data.avatar data.firstName data.lastName data.email'
        )
        .lean();
    const langInJob = jobUtils
        .getActiveLanguageTranslateJobs()
        .filter((each) => each.targetLanguage === language.languageCode);
    if (langInJob.length > 0) {
        language['onGoingAutoGenerate'] = true;
        language['jobProcess'] = langInJob[0].process;
    }
    // Activity.create({
    //     fromId: req.user._id,
    //     fromType: constantUtils.ACTIVITIES.ADMINS,
    //     toId: language.id,
    //     toType: constantUtils.ACTIVITIES.LANGUAGES,
    //     action: constantUtils.ACTIVITIES.VIEW,
    // });
    return res.json(language);
});

controller.setDefaultLanguage = handler(async (req, res) => {
    const ifLanguage = await Language.findById(req.body.id);
    if (!ifLanguage) throw '404|' + constantUtils.INVALID_ID;
    await Language.updateMany(
        {},
        {
            $set: {
                languageDefault: false,
            },
        }
    );
    await Language.findByIdAndUpdate(req.body.id, {
        $set: {
            languageDefault: true,
            lastEdited: req.user._id,
        },
    });
    Activity.create({
        fromId: req.user._id,
        fromType: constantUtils.ACTIVITIES.ADMINS,
        toId: ifLanguage.id,
        toType: constantUtils.ACTIVITIES.LANGUAGES,
        action: constantUtils.ACTIVITIES.SET_DEFAULT_LANGUAGE,
    });
    await helperUtils.updateLanguageRedis();
    return res.json({
        message: constantUtils.SUCCESS,
    });
});

controller.bingTranslateAdminKeys = handler(async (req, res) => {
    const config = await Other.config();
    const concurrentLanguageJobs =
        config?.scrapping?.translateConcurrentJobs ?? 2;
    const activeJobs = jobUtils.getActiveLanguageTranslateJobs();
    const language = await Language.findById(req.body.id);
    if (!language) throw '400|' + constantUtils.INVALID_ID;
    if (language.onGoingAutoGenerate)
        throw '400|' + constantUtils.ALREADY_IN_JOB;
    if (concurrentLanguageJobs > activeJobs.length) {
        await Language.findByIdAndUpdate(req.body.id, {
            $set: {
                lastEdited: req.user._id,
            },
        });
        jobUtils.translateFromBingScrapping(
            req.body.langKeys,
            language.languageCode,
            language._id
        );
        Activity.create({
            fromId: req.user._id,
            fromType: constantUtils.ACTIVITIES.ADMINS,
            toId: req.body.id,
            toType: constantUtils.ACTIVITIES.LANGUAGES,
            action: constantUtils.ACTIVITIES.UPDATE,
        });
    } else throw '400|' + constantUtils.MAX_JOBS_REACHED;
    return res.json({
        message: 'Initiated',
    });
});

// Mobile

controller.getMobileUserLangKeys = handler(async (req, res) => {
    const lang = await Language.findOne({
        languageCode: req?.params?.langCode,
    }).select(
        'languageCode languageName languageNativeName languageMobileUserKeys updatedAt'
    );
    if (!lang) throw '404|' + constantUtils.LANGUAGE_NOT_FOUND;
    else
        return res.json({
            languageName: lang?.languageName,
            languageNativeName: lang?.languageNativeName,
            languageCode: lang?.languageCode,
            languageKeys: lang?.languageMobileUserKeys ?? {},
            updateAt: lang?.updatedAt,
        });
});

controller.getMobilePartnerLangKeys = handler(async (req, res) => {
    const lang = await Language.findOne({
        languageCode: req?.params?.langCode,
    }).select(
        'languageCode languageName languageNativeName languageMobilePartnerKeys updatedAt'
    );
    if (!lang) throw '404|' + constantUtils.LANGUAGE_NOT_FOUND;
    else
        return res.json({
            languageName: lang?.languageName,
            languageNativeName: lang?.languageNativeName,
            languageCode: lang?.languageCode,
            languageKeys: lang?.languageMobilePartnerKeys ?? {},
            updateAt: lang?.updatedAt,
        });
});

controller.updateMobileKeys = handler(async (req, res) => {
    const ifLanguage = await Language.findOne({
        languageCode: req.body.langCode,
    });

    if (!ifLanguage) throw '404|' + constantUtils.NO_LANGUAGE_FOUND;
    let updateStuff = {};

    const languageKey =
        req?.body?.userType === 'USER'
            ? 'languageMobileUserKeys'
            : 'languageMobilePartnerKeys';

    // Check if sent duplicate key
    const keys =
        req.body.langKeys && Array.isArray(req.body.langKeys)
            ? req.body.langKeys.map((each) => each.key)
            : [];

    if (req.body.type === 'REPLACE' || req.body.type === 'PUSH') {
        var isDuplicateSent = keys.some(
            (item, idx) => keys.indexOf(item) != idx
        );
        if (isDuplicateSent) throw '400|' + constantUtils.SENT_KEY_DUPLICATE;
    }
    if (req.body.type === 'FLUSH') {
        updateStuff = {
            $set: {},
        };
        updateStuff['$set'][languageKey] = [];
    }
    if (req.body.type === 'PUSH') {
        const condition = {
            languageCode: req.body.langCode,
        };
        condition[`${languageKey}.key`] = {
            $in: keys,
        };
        const ifDuplicateKey = await Language.findOne(condition);
        if (ifDuplicateKey) throw '400|' + constantUtils.KEY_EXISTS;
        updateStuff = {
            $push: {},
        };
        updateStuff['$push'][languageKey] = req.body.langKeys;
    }
    if (req.body.type === 'REPLACE') {
        updateStuff = {
            $set: {},
        };
        updateStuff['$set'][languageKey] = req.body.langKeys;
    }
    let updatedLang = {};
    if (req?.body?.type === 'REPLACE_SINGLE') {
        if (!req.body.langKey) throw '400|' + 'langKey required';
        await Language.updateOne(
            {
                _id: ifLanguage._id,
                'languageMobileUserKeys.key': req.body.langKey,
            },
            {
                $set: {
                    'languageMobileUserKeys.$.key': req.body.langKeys[0]?.key,
                    'languageMobileUserKeys.$.value':
                        req.body.langKeys[0]?.value,
                },
            }
        );
        updatedLang = await Language.findById(ifLanguage._id);
    } else
        updatedLang = await Language.findByIdAndUpdate(
            ifLanguage._id,
            updateStuff,
            {
                new: true,
            }
        );
    await helperUtils.updateLanguageRedis();
    return res.json({
        languageName: updatedLang?.languageName,
        languageNativeName: updatedLang?.languageNativeName,
        languageCode: updatedLang?.languageCode,
        languageKeys:
            req?.body?.userType === 'USER'
                ? updatedLang?.languageMobileUserKeys
                : updatedLang?.languageMobilePartnerKeys,
        updatedAt: updatedLang?.updatedAt,
    });
});

controller.getLanguage = handler(async (req, res) => {
    const ifLanguage = await Language.findById(req?.body?.id);
    if (!ifLanguage) throw '404|' + constantUtils.LANGUAGE_NOT_FOUND;
    else {
        Activity.create({
            fromId: req.user._id,
            fromType: constantUtils.ACTIVITIES.ADMINS,
            toId: ifLanguage.id,
            toType: constantUtils.ACTIVITIES.LANGUAGES,
            action: constantUtils.ACTIVITIES.VIEW,
        });
        return res.json(ifLanguage);
    }
});

module.exports = controller;
