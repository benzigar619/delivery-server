const express = require('express');
const router = express.Router();

const multer = require('multer');
const upload = multer();

const controller = require('../controllers/ratings.controller');
const { adminTokenVerify, userTokenVerify, partnerTokenVerify } = require('../utils/auth.utils');
const { validateBody } = require('../utils/errors.utils');
const validations = require('../validations/ratings.validation');
const { paginationParams } = require('../utils/helper.utils');

// ---------------- MOBILE --------------

router.post('/uploadImage', userTokenVerify, upload.single('image'), controller.uploadImage);
router.post('/removeImage', userTokenVerify, validateBody(validations.removeImage), controller.removeImage);

// Rating
router.post('/rateOrder', userTokenVerify, validateBody(validations.rateOrder), controller.rateOrder);
router.post('/rateUser', partnerTokenVerify, validateBody(validations.rateUser), controller.rateUser);
router.post('/ratePartner', userTokenVerify, validateBody(validations.ratePartner), controller.ratePartner);
router.post('/getProductRatings', paginationParams, validateBody(validations.getProductRatings), controller.getProductRatings);

module.exports = router;
