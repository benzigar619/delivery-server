const express = require('express');
const router = express.Router();

const controller = require('../controllers/logs.controller');

const { validateBody } = require('../utils/errors.utils');
const { adminTokenVerify } = require('../utils/auth.utils');
const { paginationParams } = require('../utils/helper.utils');

// Logins
router.post('/getAllLogs', adminTokenVerify, paginationParams, controller.getAllLogs);
router.post('/getApiLogCount', paginationParams, controller.getApiLogCount);

module.exports = router;
