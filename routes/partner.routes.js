const express = require('express');
const router = express.Router();

const multer = require('multer');
const upload = multer();

const controller = require('../controllers/partner.controller');
const { adminTokenVerify, partnerTokenVerify } = require('../utils/auth.utils');
const { validateBody } = require('../utils/errors.utils');
const validations = require('../validations/partners.validation');
const { paginationParams } = require('../utils/helper.utils');

// Multi Image Upload
const uploadPartnerImage = upload.fields([
    { name: 'avatar' },
    { name: 'frontImage' },
    { name: 'backImage' },
    { name: 'leftImage' },
    { name: 'rightImage' },
    { name: 'partnerDocument' },
    { name: 'vehicleDocument' },
]);

// --------- ADMIN PANEL -------------------
router.post('/getAllPartners', adminTokenVerify, paginationParams, validateBody(validations.getAllPartners), controller.getAllPartners);
router.post('/getViewPartner', adminTokenVerify, validateBody(validations.getViewPartner), controller.getViewPartner);
router.post('/getEditPartner', adminTokenVerify, controller.getEditPartner);
router.post('/addPartner', adminTokenVerify, validateBody(validations.addPartner), controller.addPartner);
router.post('/uploadPartnerImage', adminTokenVerify, uploadPartnerImage, controller.uploadPartnerImage);
router.post('/getPartnerAvailableProducts', adminTokenVerify, controller.getPartnerAvailableProducts);
router.post('/addPartnerAvailableProduct', adminTokenVerify, validateBody(validations.addPartnerAvailableProduct), controller.addPartnerAvailableProduct);
router.post('/partnerProductToggleStatus', adminTokenVerify, validateBody(validations.partnerProductToggleStatus), controller.partnerProductToggleStatus);
router.post('/partnerProductAddStock', adminTokenVerify, validateBody(validations.partnerProductAddStock), controller.partnerProductAddStock);
router.post('/reFillRequestStatusChange', adminTokenVerify, validateBody(validations.reFillRequestStatusChange), controller.reFillRequestStatusChange);
router.post('/getPartnersInCity', adminTokenVerify, controller.getPartnersInCity);

// Forgot Password
router.post('/forgotPassword', controller.forgotPassword);
router.post('/forgotPasswordEmailGetDetails', controller.forgotPasswordEmailGetDetails);
router.post('/forgotPasswordEmailSubmit', validateBody(validations.forgotPasswordEmailSubmit), controller.forgotPasswordEmailSubmit);

// --------- MOBILE-------------------------
router.post('/emailPasswordLogin', validateBody(validations.emailPasswordLogin), controller.emailPasswordLogin);
router.post('/checkLogin', partnerTokenVerify, controller.checkLogin);
router.post('/getProfile', partnerTokenVerify, validateBody(validations.getProfile), controller.getProfile);
router.post('/getDashboard', partnerTokenVerify, controller.getDashboard);
router.post('/updateLanguage', partnerTokenVerify, controller.updateLanguage);

// Location
router.post('/toggleOnlineOffline', partnerTokenVerify, validateBody(validations.toggleOnlineOffline), controller.toggleOnlineOffline);
router.post('/updateLocation', partnerTokenVerify, validateBody(validations.updateLocation), controller.updateLocation);

// Available Products
router.post('/changePassword', partnerTokenVerify, validateBody(validations.changePassword), controller.changePassword);
router.post('/forgotPassword', partnerTokenVerify, controller.forgotPassword);
router.post('/availableProductStatusChange', partnerTokenVerify, validateBody(validations.availableProductStatusChange), controller.availableProductStatusChange);
router.post('/getProducts', partnerTokenVerify, controller.getProducts);
router.post('/requestReFill', partnerTokenVerify, controller.requestReFill);

// Orders
// router.post('/getOnGoingOrders', partnerTokenVerify, controller.getOnGoingOrders);

// Order Process
router.post('/orderRequestedPartnerStatusChange', partnerTokenVerify, validateBody(validations.orderRequestedPartnerStatusChange), controller.orderRequestedPartnerStatusChange);
router.post('/acceptOrder', partnerTokenVerify, validateBody(validations.acceptOrder), controller.acceptOrder);
router.post('/startOrder', partnerTokenVerify, validateBody(validations.startOrder), controller.startOrder);
router.post('/arriveOrder', partnerTokenVerify, validateBody(validations.arriveOrder), controller.arriveOrder);
router.post('/deliverOrder', partnerTokenVerify, validateBody(validations.deliverOrder), controller.deliverOrder);
router.post('/cancelOrder', partnerTokenVerify, validateBody(validations.cancelOrder), controller.cancelOrder);

// Order Details
router.post('/getOrderDetail', partnerTokenVerify, validateBody(validations.getOrderDetail), controller.getOrderDetail);
router.post('/getOrdersList', partnerTokenVerify, paginationParams, validateBody(validations.getOrdersList), controller.getOrdersList);
module.exports = router;
